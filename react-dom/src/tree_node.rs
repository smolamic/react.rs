#[derive(Debug)]
pub(crate) struct TreeNode<T> {
    pub(crate) value: T,
    pub(crate) children: Vec<TreeNode<T>>,
}

impl<T> TreeNode<T> {
    pub(crate) fn new(value: T) -> Self {
        Self {
            value,
            children: Vec::new(),
        }
    }

    pub(crate) fn with_children(value: T, children: Vec<Self>) -> Self {
        Self { value, children }
    }
}
