use react_html::HtmlElement;

pub(crate) struct Update {
    pub(crate) element: HtmlElement,
    pub(crate) sibling: Option<web_sys::Node>,
}
