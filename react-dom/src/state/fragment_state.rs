use crate::{
    state::{self, State},
    transition::Transition,
    tree_node::TreeNode,
};
use itertools::EitherOrBoth;
use react_html::HtmlElement;

#[derive(Debug)]
pub(crate) struct FragmentState {
    pub(crate) position: Option<web_sys::Node>,
}

impl FragmentState {
    pub(crate) fn new() -> Self {
        Self { position: None }
    }

    pub(crate) fn create(
        children: Vec<HtmlElement>,
        sibling: Option<web_sys::Node>,
    ) -> (Transition, Vec<state::MapStateInput>) {
        let children = children
            .into_iter()
            .map(|child| EitherOrBoth::Right((child, sibling.clone())))
            .collect();

        (Transition::StayFragment, children)
    }
    pub(crate) fn update(
        self,
        next: HtmlElement,
        sibling: Option<web_sys::Node>,
        children: Vec<TreeNode<State>>,
    ) -> (Transition, Vec<state::MapStateInput>) {
        match next {
            HtmlElement::Fragment(next_fragment) => {
                let children = state::zip_children(children, next_fragment);

                (Transition::StayFragment, children)
            }

            _ => self.replace(next, sibling, children),
        }
    }

    fn replace(
        self,
        next: HtmlElement,
        sibling: Option<web_sys::Node>,
        children: Vec<TreeNode<State>>,
    ) -> (Transition, Vec<state::MapStateInput>) {
        let mut children: Vec<state::MapStateInput> =
            children.into_iter().map(EitherOrBoth::Left).collect();

        children.push(EitherOrBoth::Right((next, sibling)));

        (Transition::Composite, children)
    }

    pub(crate) fn remove(
        self,
        children: Vec<TreeNode<State>>,
    ) -> (Transition, Vec<state::MapStateInput>) {
        (
            Transition::Composite,
            children.into_iter().map(EitherOrBoth::Left).collect(),
        )
    }

    pub(crate) fn position(&self) -> Option<&web_sys::Node> {
        self.position.as_ref()
    }
}
