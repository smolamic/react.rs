mod component_state;
mod event_listener_handle;
mod fragment_state;
mod node_state;
mod text_state;

use crate::{map_tree::map_tree, transition::Transition, tree_node::TreeNode, Context};
pub(crate) use component_state::ComponentState;
pub(crate) use event_listener_handle::EventListenerHandle;
pub(crate) use fragment_state::FragmentState;
use itertools::{EitherOrBoth, Itertools};
pub(crate) use node_state::NodeState;
use react_html::HtmlElement;
pub(crate) use text_state::TextState;

#[derive(Debug)]
pub(crate) enum State {
    Node(NodeState),
    Text(TextState),
    Component(ComponentState),
    Fragment(FragmentState),
}

impl State {
    pub(crate) fn create(
        element: HtmlElement,
        sibling: Option<web_sys::Node>,
        cx: &Context,
    ) -> (Transition, Vec<MapStateInput>) {
        match element {
            HtmlElement::Node(node) => NodeState::create(node, sibling),
            HtmlElement::Text(text) => TextState::create(text, sibling),
            HtmlElement::Component(component) => ComponentState::create(component, sibling, cx),
            HtmlElement::Fragment(children) => FragmentState::create(children, sibling),
        }
    }

    pub(crate) fn update(
        self,
        next: HtmlElement,
        sibling: Option<web_sys::Node>,
        children: Vec<TreeNode<State>>,
    ) -> (Transition, Vec<MapStateInput>) {
        match self {
            Self::Node(state) => state.update(next, sibling, children),
            Self::Text(state) => state.update(next, sibling, children),
            Self::Component(state) => state.update(next, sibling, children),
            Self::Fragment(state) => state.update(next, sibling, children),
        }
    }

    pub(crate) fn remove(self, children: Vec<TreeNode<State>>) -> (Transition, Vec<MapStateInput>) {
        match self {
            Self::Node(state) => state.remove(children),
            Self::Text(state) => state.remove(children),
            Self::Component(state) => state.remove(children),
            Self::Fragment(state) => state.remove(children),
        }
    }

    pub(crate) fn position(&self) -> Option<&web_sys::Node> {
        match self {
            Self::Node(state) => state.position(),
            Self::Text(state) => state.position(),
            Self::Component(state) => state.position(),
            Self::Fragment(state) => state.position(),
        }
    }
}

pub(crate) type MapStateInput = EitherOrBoth<TreeNode<State>, (HtmlElement, Option<web_sys::Node>)>;

pub(crate) fn map_state_to_transition(input: MapStateInput, cx: &Context) -> TreeNode<Transition> {
    map_tree(
        input,
        |input| match input {
            EitherOrBoth::Left(node) => node.value.remove(node.children),

            EitherOrBoth::Both(node, (element, sibling)) => {
                node.value.update(element, sibling, node.children)
            }

            EitherOrBoth::Right((element, sibling)) => State::create(element, sibling, cx),
        },
        TreeNode::with_children,
    )
}

pub(crate) fn zip_children(
    nodes: Vec<TreeNode<State>>,
    elements: Vec<HtmlElement>,
) -> Vec<MapStateInput> {
    let length = std::cmp::max(nodes.len(), elements.len());
    let mut children = Vec::with_capacity(length);

    let mut iterator = nodes
        .into_iter()
        .zip_longest(elements.into_iter())
        .peekable();

    while let Some(item) = iterator.next() {
        let sibling = iterator
            .peek()
            .and_then(|next_item| next_item.as_ref().left())
            .map(|next_node| next_node.value.position())
            .flatten()
            .cloned();

        children.push(item.map_right(|element| (element, sibling)));
    }

    children
}
