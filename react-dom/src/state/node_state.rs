use crate::{
    state::{self, State},
    transition::{self, Transition},
    tree_node::TreeNode,
};
use itertools::EitherOrBoth;
use react_html::{HtmlElement, HtmlNode};
use std::{collections::HashMap, sync::Arc};

#[derive(Debug)]
pub(crate) struct NodeState {
    pub(crate) node: Arc<HtmlNode>,
    pub(crate) web_element: web_sys::Element,
    pub(crate) event_listener_handles: HashMap<String, state::EventListenerHandle>,
}

impl NodeState {
    pub(crate) fn from(
        node: Arc<HtmlNode>,
        web_element: web_sys::Element,
        event_listener_handles: HashMap<String, state::EventListenerHandle>,
    ) -> Self {
        Self {
            node,
            web_element,
            event_listener_handles,
        }
    }

    pub(crate) fn create(
        node: Arc<HtmlNode>,
        sibling: Option<web_sys::Node>,
    ) -> (Transition, Vec<state::MapStateInput>) {
        let transition = Transition::CreateNode(transition::CreateNodeTransition::from(
            node.clone(),
            sibling,
        ));
        let children = node
            .children()
            .iter()
            .map(|child| EitherOrBoth::Right((child.clone(), None)))
            .collect();

        (transition, children)
    }

    pub(crate) fn update(
        self,
        next: HtmlElement,
        sibling: Option<web_sys::Node>,
        children: Vec<TreeNode<State>>,
    ) -> (Transition, Vec<state::MapStateInput>) {
        match next {
            HtmlElement::Node(next_node) => {
                if self.node.tag_name() == next_node.tag_name() {
                    let children = state::zip_children(children, next_node.children().clone());
                    (
                        Transition::StayNode(transition::StayNodeTransition {
                            node: self.node,
                            web_element: self.web_element,
                            next: next_node,
                            event_listener_handles: self.event_listener_handles,
                        }),
                        children,
                    )
                } else {
                    self.replace(HtmlElement::Node(next_node), sibling, children)
                }
            }

            _ => self.replace(next, sibling, children),
        }
    }

    fn replace(
        self,
        next: HtmlElement,
        sibling: Option<web_sys::Node>,
        children: Vec<TreeNode<State>>,
    ) -> (Transition, Vec<state::MapStateInput>) {
        (
            Transition::Composite,
            vec![
                EitherOrBoth::Left(TreeNode::with_children(State::Node(self), children)),
                EitherOrBoth::Right((next.clone(), sibling)),
            ],
        )
    }

    pub(crate) fn remove(
        self,
        children: Vec<TreeNode<State>>,
    ) -> (Transition, Vec<state::MapStateInput>) {
        let transition =
            Transition::RemoveNode(transition::RemoveNodeTransition::from(self.web_element));

        let children = children.into_iter().map(EitherOrBoth::Left).collect();

        (transition, children)
    }

    pub(crate) fn position(&self) -> Option<&web_sys::Node> {
        Some(&self.web_element)
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;
    use crate::{tests::create_element, transition::Transition};
    use react_html::tag;
    use wasm_bindgen_test::*;

    fn make_div_state() -> NodeState {
        let element = create_element("div");
        NodeState {
            node: tag::div().build().into(),
            web_element: element,
            event_listener_handles: HashMap::new(),
        }
    }

    #[wasm_bindgen_test]
    fn gets_replaced_by_fragment() {
        let state = make_div_state();

        if let (Transition::Composite, children) = state.update(Vec::new().into(), None, Vec::new())
        {
            assert_eq!(children.len(), 2);
        } else {
            panic!("Wrong update output");
        }
    }

    #[wasm_bindgen_test]
    fn updates_its_properties() {
        let state = make_div_state();

        if let (Transition::StayNode(_), children) = state.update(
            tag::div().attr(("class", "test")).build().into(),
            None,
            Vec::new(),
        ) {
            assert_eq!(0, children.len());
        } else {
            panic!("Wrong update output");
        }
    }
}
