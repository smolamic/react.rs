use crate::Context;
use crate::{
    state::{self, State},
    transition::{self, Transition},
    tree_node::TreeNode,
};
use itertools::EitherOrBoth;
use react_core::{
    component::{AnyComponent, ComponentDiff},
    Hook,
};
use react_html::{HtmlElement, HtmlNode};
use std::sync::Arc;

#[derive(Debug)]
pub(crate) struct ComponentState {
    pub(crate) component: Arc<dyn AnyComponent<HtmlNode>>,
    pub(crate) rendered_element: HtmlElement,
    pub(crate) hook: Hook,
    pub(crate) position: Option<web_sys::Node>,
}

impl ComponentState {
    pub(crate) fn from(
        component: Arc<dyn AnyComponent<HtmlNode>>,
        rendered_element: HtmlElement,
        hook: Hook,
    ) -> Self {
        Self {
            component,
            rendered_element,
            hook,
            position: None,
        }
    }

    pub(crate) fn create(
        component: Arc<dyn AnyComponent<HtmlNode>>,
        sibling: Option<web_sys::Node>,
        cx: &Context,
    ) -> (Transition, Vec<state::MapStateInput>) {
        let mut hook = Hook::new(cx.spawner.clone(), cx.signal_sender.clone());

        let rendered_element = component.render(&mut hook);

        let transition = Transition::StayComponent(transition::StayComponentTransition::from(
            component,
            rendered_element.clone(),
            hook,
        ));

        (
            transition,
            vec![EitherOrBoth::Right((rendered_element, sibling))],
        )
    }

    pub(crate) fn update(
        mut self,
        next: HtmlElement,
        sibling: Option<web_sys::Node>,
        mut children: Vec<TreeNode<State>>,
    ) -> (Transition, Vec<state::MapStateInput>) {
        webdbg!("update component {:?} with {:?}", &self.component, &next);
        debug_assert!(children.len() == 1);

        let child_tree_node = children
            .pop()
            .expect("Invariant Violation: Components must have a single child");

        match next {
            HtmlElement::Component(next_component) => {
                match self.component.compare(next_component.as_any()) {
                    ComponentDiff::Equal => {
                        webdbg!("components are equal");
                        let child_element = if self.hook.update() {
                            next_component.render(&mut self.hook)
                        } else {
                            self.rendered_element
                        };

                        let children = vec![EitherOrBoth::Both(
                            child_tree_node,
                            (child_element.clone(), sibling),
                        )];

                        let transition =
                            Transition::stay_component(self.component, child_element, self.hook);

                        (transition, children)
                    }

                    ComponentDiff::NewProps => {
                        webdbg!("component got new props");
                        self.hook.update();
                        let child_element = next_component.render(&mut self.hook);

                        let children = vec![EitherOrBoth::Both(
                            child_tree_node,
                            (child_element.clone(), sibling),
                        )];

                        let transition =
                            Transition::stay_component(next_component, child_element, self.hook);

                        (transition, children)
                    }
                    ComponentDiff::NewType => {
                        webdbg!("new component type");
                        self.replace(HtmlElement::Component(next_component), sibling, children)
                    }
                }
            }

            _ => self.replace(next, sibling, children),
        }
    }

    fn replace(
        self,
        next: HtmlElement,
        sibling: Option<web_sys::Node>,
        children: Vec<TreeNode<State>>,
    ) -> (Transition, Vec<state::MapStateInput>) {
        // all children will be removed
        let mut children: Vec<state::MapStateInput> =
            children.into_iter().map(EitherOrBoth::Left).collect();

        // the element to replace this will be created
        children.push(EitherOrBoth::Right((next, sibling)));

        (Transition::Composite, children)
    }

    pub(crate) fn remove(
        self,
        children: Vec<TreeNode<State>>,
    ) -> (Transition, Vec<state::MapStateInput>) {
        // all children will be removed
        let children = children.into_iter().map(EitherOrBoth::Left).collect();

        (Transition::Composite, children)
    }

    pub(crate) fn position(&self) -> Option<&web_sys::Node> {
        self.position.as_ref()
    }
}
