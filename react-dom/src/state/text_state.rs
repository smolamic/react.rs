use crate::{
    state::{self, State},
    transition::{self, Transition},
    tree_node::TreeNode,
};
use itertools::EitherOrBoth;
use react_html::HtmlElement;
use std::sync::Arc;

#[derive(Debug)]
pub(crate) struct TextState {
    pub(crate) text: Arc<String>,
    pub(crate) web_text: web_sys::Text,
}

impl TextState {
    pub(crate) fn from(text: Arc<String>, web_text: web_sys::Text) -> Self {
        Self { text, web_text }
    }

    pub(crate) fn create(
        text: Arc<String>,
        sibling: Option<web_sys::Node>,
    ) -> (Transition, Vec<state::MapStateInput>) {
        let transition =
            Transition::CreateText(transition::CreateTextTransition::from(text, sibling));

        (transition, Vec::new())
    }

    pub(crate) fn update(
        self,
        next: HtmlElement,
        sibling: Option<web_sys::Node>,
        children: Vec<TreeNode<State>>,
    ) -> (Transition, Vec<state::MapStateInput>) {
        debug_assert!(children.is_empty());

        webdbg!("update text {:#?} {:#?}", self.text, next);

        match next {
            HtmlElement::Text(next_text) => {
                if self.text == next_text {
                    (
                        Transition::StayText(transition::StayTextTransition::from(
                            self.text,
                            self.web_text,
                        )),
                        Vec::new(),
                    )
                } else {
                    (
                        Transition::Composite,
                        vec![
                            EitherOrBoth::Left(TreeNode::new(State::Text(self))),
                            EitherOrBoth::Right((HtmlElement::Text(next_text), sibling)),
                        ],
                    )
                }
            }

            _ => (
                Transition::Composite,
                vec![
                    EitherOrBoth::Left(TreeNode::new(State::Text(self))),
                    EitherOrBoth::Right((next, sibling)),
                ],
            ),
        }
    }

    pub(crate) fn remove(
        self,
        children: Vec<TreeNode<State>>,
    ) -> (Transition, Vec<state::MapStateInput>) {
        debug_assert!(children.is_empty());

        let transition =
            Transition::RemoveText(transition::RemoveTextTransition::from(self.web_text));

        (transition, Vec::new())
    }

    pub(crate) fn position(&self) -> Option<&web_sys::Node> {
        Some(&self.web_text)
    }
}
