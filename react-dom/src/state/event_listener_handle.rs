use react_html::{event_handler::AnyEventHandler, Event};
use wasm_bindgen::closure::Closure;
use wasm_bindgen::JsCast;

struct WebEvent;

impl EventImpl for WebEvent {}

#[derive(Debug)]
pub(crate) struct EventListenerHandle {
    pub(crate) closure: wasm_bindgen::closure::Closure<dyn Fn(web_sys::Event)>,
    pub(crate) event_listener: web_sys::EventListener,
}

impl EventListenerHandle {
    pub(crate) fn add(
        web_element: &web_sys::Element,
        type_: &str,
        event_handler: Box<dyn AnyEventHandler>,
    ) -> EventListenerHandle {
        let closure = Closure::wrap(Box::new(move |_event: web_sys::Event| {
            let react_event = Event;
            event_handler.handle_event(react_event);
        }) as Box<dyn Fn(web_sys::Event)>);

        let event_listener = web_sys::EventListener::new();
        web_element
            .add_event_listener_with_callback(&type_, closure.as_ref().unchecked_ref())
            .expect("Failed to add Event Listener");

        EventListenerHandle {
            closure,
            event_listener,
        }
    }

    pub(crate) fn remove(self, web_element: &web_sys::Element, type_: &str) {
        web_element
            .remove_event_listener_with_callback(&type_, self.closure.as_ref().unchecked_ref())
            .expect("Failed to remove Event Listener");
    }
}
