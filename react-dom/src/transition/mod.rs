mod create_node;
mod create_text;
mod remove_node;
mod remove_text;
mod stay_component;
mod stay_node;
mod stay_text;
mod value_to_js;

use crate::map_tree::map_tree;
use crate::{
    state::{self, State},
    tree_node::TreeNode,
};
pub(crate) use create_node::CreateNodeTransition;
pub(crate) use create_text::CreateTextTransition;
use react_core::{component::AnyComponent, Hook};
use react_html::{HtmlElement, HtmlNode};
pub(crate) use remove_node::RemoveNodeTransition;
pub(crate) use remove_text::RemoveTextTransition;
pub(crate) use stay_component::StayComponentTransition;
pub(crate) use stay_node::StayNodeTransition;
pub(crate) use stay_text::StayTextTransition;
use std::sync::Arc;
pub(crate) use value_to_js::value_to_js;

#[derive(Debug)]
pub(crate) enum Transition {
    CreateNode(CreateNodeTransition),
    StayNode(StayNodeTransition),
    RemoveNode(RemoveNodeTransition),
    CreateText(CreateTextTransition),
    StayText(StayTextTransition),
    RemoveText(RemoveTextTransition),
    StayComponent(StayComponentTransition),
    StayFragment,
    Composite,
}

impl Transition {
    pub(crate) fn apply(
        self,
        parent: web_sys::Node,
        children: Vec<TreeNode<Transition>>,
    ) -> (Option<State>, Vec<MapTransitionInput>) {
        match self {
            Self::CreateNode(tr) => tr.apply(parent, children),
            Self::StayNode(tr) => tr.apply(children),
            Self::RemoveNode(tr) => tr.apply(parent, children),
            Self::CreateText(tr) => tr.apply(parent, children),
            Self::StayText(tr) => tr.apply(children),
            Self::RemoveText(tr) => tr.apply(parent, children),
            Self::StayComponent(tr) => tr.apply(parent, children),
            Self::StayFragment => (
                Some(State::Fragment(state::FragmentState::new())),
                children
                    .into_iter()
                    .map(|node| (node, parent.clone()))
                    .collect(),
            ),
            Self::Composite => (
                None,
                children
                    .into_iter()
                    .map(|node| (node, parent.clone()))
                    .collect(),
            ),
        }
    }

    pub(crate) fn create_node(node: Arc<HtmlNode>, sibling: Option<web_sys::Node>) -> Self {
        Self::CreateNode(CreateNodeTransition::from(node, sibling))
    }

    pub(crate) fn stay_component(
        component: Arc<dyn AnyComponent<HtmlNode>>,
        rendered_element: HtmlElement,
        hook: Hook,
    ) -> Self {
        Self::StayComponent(StayComponentTransition::from(
            component,
            rendered_element,
            hook,
        ))
    }
}

pub(crate) type MapTransitionInput = (TreeNode<Transition>, web_sys::Node);

pub(crate) fn map_transition_to_state(input: MapTransitionInput) -> Option<TreeNode<State>> {
    map_tree(
        input,
        |(node, parent)| node.value.apply(parent, node.children),
        |state, children| -> Option<TreeNode<State>> {
            let children = children.into_iter().flatten().collect();
            match state {
                Some(State::Node(node_state)) => {
                    Some(TreeNode::with_children(State::Node(node_state), children))
                }

                Some(State::Text(text_state)) => {
                    Some(TreeNode::with_children(State::Text(text_state), children))
                }

                Some(State::Component(mut component_state)) => {
                    let position = children
                        .iter()
                        .find_map(|child| child.value.position())
                        .cloned();

                    component_state.position = position;

                    Some(TreeNode::with_children(
                        State::Component(component_state),
                        children,
                    ))
                }

                Some(State::Fragment(mut fragment_state)) => {
                    let position = children
                        .iter()
                        .find_map(|child| child.value.position())
                        .cloned();

                    fragment_state.position = position;

                    Some(TreeNode::with_children(
                        State::Fragment(fragment_state),
                        children,
                    ))
                }

                None => children.into_iter().nth(0),
            }
        },
    )
}

#[cfg(test)]
pub mod tests {
    use super::*;
    use crate::{
        state::{self, State},
        tests::{create_context, create_element, create_root},
    };
    use itertools::EitherOrBoth;
    use react_html::{tag, HtmlElement};
    use std::sync::Arc;
    use wasm_bindgen_test::*;

    #[wasm_bindgen_test]
    fn create_and_apply_text() {
        let transition = Transition::CreateText(CreateTextTransition::from(
            Arc::new("text".to_string()),
            None,
        ));

        let parent = create_element("div");

        let state = transition.apply(parent.clone().into(), Vec::new());

        match state {
            (Some(State::Text(state::TextState { text, .. })), children) => {
                assert_eq!("text", text.as_ref());

                assert_eq!("text", parent.inner_html());

                assert!(children.is_empty());
            }
            _ => panic!("Applied CreateText, received some other state"),
        }
    }

    #[wasm_bindgen_test]
    fn create_deeply_nested_divs() {
        let cx = create_context();
        let root = create_root();

        let mut element: HtmlElement = "test".into();

        for _ in 0..500 {
            element = tag::div().children(vec![element]).build().into();
        }

        let root_node = state::map_state_to_transition(EitherOrBoth::Right((element, None)), &cx);

        match &root_node.value {
            Transition::CreateNode(tr) => {
                assert_eq!(tr.node.tag_name(), "div");
            }
            _ => panic!("expected to receive CreateNode transition holding a div"),
        }

        let state = map_transition_to_state((root_node, root.clone().into()));

        match state {
            Some(root_node) => match root_node.value {
                State::Node(state) => {
                    assert_eq!(state.node.tag_name(), "div");
                }
                _ => panic!("expected to receive NodeState"),
            },
            None => panic!("expected to receive some state"),
        }
    }
}
