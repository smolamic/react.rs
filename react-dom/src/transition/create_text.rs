use crate::{
    state::{self, State},
    transition::{self, Transition},
    tree_node::TreeNode,
};
use std::sync::Arc;

#[derive(Debug)]
pub(crate) struct CreateTextTransition {
    pub(crate) text: Arc<String>,
    pub(crate) position: Option<web_sys::Node>,
}

impl CreateTextTransition {
    pub(crate) fn from(text: Arc<String>, position: Option<web_sys::Node>) -> Self {
        Self { text, position }
    }

    pub(crate) fn apply(
        self,
        parent: web_sys::Node,
        children: Vec<TreeNode<Transition>>,
    ) -> (Option<State>, Vec<transition::MapTransitionInput>) {
        debug_assert!(children.is_empty());

        let web_text = self.build_text();

        parent
            .insert_before(&web_text, self.position.as_ref())
            .expect("failed to append text");

        (
            Some(State::Text(state::TextState::from(self.text, web_text))),
            Vec::new(),
        )
    }

    pub(crate) fn build_text(&self) -> web_sys::Text {
        web_sys::window()
            .expect("Failed to get window")
            .document()
            .expect("Failed to get document")
            .create_text_node(&self.text[..])
    }
}
