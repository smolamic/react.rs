use crate::{
    state::State,
    transition::{self, Transition},
    tree_node::TreeNode,
};

#[derive(Debug)]
pub(crate) struct RemoveTextTransition {
    pub(crate) web_text: web_sys::Text,
}

impl RemoveTextTransition {
    pub(crate) fn from(web_text: web_sys::Text) -> Self {
        Self { web_text }
    }

    pub(crate) fn apply(
        self,
        parent: web_sys::Node,
        children: Vec<TreeNode<Transition>>,
    ) -> (Option<State>, Vec<transition::MapTransitionInput>) {
        debug_assert!(children.is_empty());

        parent
            .remove_child(&self.web_text)
            .expect("Failed to remove text");

        (None, Vec::new())
    }
}
