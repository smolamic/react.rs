use crate::transition::Transition;

pub(crate) struct RemoveComponentTransition {
    pub(crate) child: Box<Transition>,
}

impl RemoveComponentTransition {
    pub(crate) fn from(child: Box<Transition>) -> Self {
        Self { child }
    }

    pub(crate) fn apply(self, parent: &web_sys::Node) {
        if self.child.apply(parent).is_some() {
            panic!("Failed to remove component");
        }
    }
}
