use crate::{
    state::{self, State},
    transition::{self, Transition},
    tree_node::TreeNode,
};
use react_html::HtmlNode;
use std::{collections::HashMap, sync::Arc};
use wasm_bindgen::JsValue;

#[derive(Debug)]
pub(crate) struct CreateNodeTransition {
    pub(crate) node: Arc<HtmlNode>,
    pub(crate) sibling: Option<web_sys::Node>,
}

impl CreateNodeTransition {
    pub(crate) fn from(node: Arc<HtmlNode>, sibling: Option<web_sys::Node>) -> Self {
        Self { node, sibling }
    }

    pub(crate) fn build_element(&self) -> web_sys::Element {
        web_sys::window()
            .expect("Failed to get window")
            .document()
            .expect("Failed to get document")
            .create_element(&self.node.tag_name())
            .unwrap_or_else(|_| panic!("Failed to create tag {}", &self.node.tag_name()))
    }

    pub(crate) fn apply(
        self,
        parent: web_sys::Node,
        children: Vec<TreeNode<Transition>>,
    ) -> (Option<State>, Vec<transition::MapTransitionInput>) {
        let web_element = self.build_element();

        for (key, value) in self.node.attributes() {
            web_sys::console::log_3(
                &JsValue::from_str("set"),
                &JsValue::from_str(key),
                &JsValue::from_str(value),
            );
            web_element
                .set_attribute(key, value)
                .expect("Failed to set attribute");
        }

        for (key, value) in self.node.properties() {
            js_sys::Reflect::set(
                &web_element,
                &key[..].into(),
                &transition::value_to_js(value),
            );
        }

        let mut event_listener_handles = HashMap::new();
        for (type_, event_handler) in self.node.event_handlers() {
            let handle =
                state::EventListenerHandle::add(&web_element, type_, event_handler.clone_boxed());
            event_listener_handles.insert(type_.to_string(), handle);
        }

        parent
            .insert_before(&web_element, self.sibling.as_ref())
            .expect("Failed to mount Node");

        let children = children
            .into_iter()
            .map(|node| (node, web_element.clone().into()))
            .collect();

        (
            Some(State::Node(state::NodeState::from(
                self.node,
                web_element,
                event_listener_handles,
            ))),
            children,
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{tests::create_root, transition::Transition};
    use react_html::{tag, Callback};
    use std::{cell::Cell, rc::Rc};
    use wasm_bindgen_test::*;
    use web_sys::MouseEvent;

    #[wasm_bindgen_test]
    fn it_adds_an_event_listener() {
        let click_count = Rc::new(Cell::new(0));
        let click_count_clone = click_count.clone();
        let transition = Transition::CreateNode(CreateNodeTransition::from(
            Arc::new(
                tag::button()
                    .id("button")
                    .on((
                        "click",
                        Callback::new(move |_| {
                            click_count_clone.set(click_count_clone.get() + 1);
                        }),
                    ))
                    .build(),
            ),
            None,
        ));

        let root = create_root();
        let _state = transition.apply(root.clone().into(), Vec::new());

        let button = root.query_selector("#button").unwrap().unwrap();

        button
            .dispatch_event(&MouseEvent::new("click").unwrap())
            .unwrap();

        assert_eq!(click_count.get(), 1);
    }
}
