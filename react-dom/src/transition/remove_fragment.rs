use crate::transition::Transition;

pub(crate) struct RemoveFragmentTransition {
    pub(crate) children: Vec<Transition>,
}

impl RemoveFragmentTransition {
    pub(crate) fn apply(self, parent: &web_sys::Node) {
        self.children.into_iter().for_each(|child| {
            if child.apply(parent).is_some() {
                panic!("Failed to remove Fragment: Child was not removed");
            }
        });
    }
}
