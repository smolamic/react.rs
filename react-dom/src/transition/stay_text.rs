use crate::{
    state::{self, State},
    transition::{self, Transition},
    tree_node::TreeNode,
};
use std::sync::Arc;

#[derive(Debug)]
pub(crate) struct StayTextTransition {
    text: Arc<String>,
    web_text: web_sys::Text,
}

impl StayTextTransition {
    pub(crate) fn from(text: Arc<String>, web_text: web_sys::Text) -> Self {
        Self { text, web_text }
    }

    pub(crate) fn apply(
        self,
        children: Vec<TreeNode<Transition>>,
    ) -> (Option<State>, Vec<transition::MapTransitionInput>) {
        debug_assert!(children.is_empty());

        (
            Some(State::Text(state::TextState::from(
                self.text,
                self.web_text,
            ))),
            Vec::new(),
        )
    }
}
