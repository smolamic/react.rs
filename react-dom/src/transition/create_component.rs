use crate::{state, transition::Transition, Context, Hook};
use react_core::component::AnyComponent;
use react_html::{HtmlElement, HtmlNode};
use std::{future::Future, pin::Pin, sync::Arc};

pub(crate) struct CreateComponentTransition {
    component: Arc<dyn AnyComponent<HtmlNode<Hook>, Hook>>,
    child: Box<Transition>,
    child_element: HtmlElement<Hook>,
    hook: Hook,
}

impl CreateComponentTransition {
    pub(crate) fn from(
        component: Arc<dyn AnyComponent<HtmlNode<Hook>, Hook>>,
        position: Option<web_sys::Node>,
        cx: &mut Context,
    ) -> Pin<Box<dyn Future<Output = Self>>> {
        let mut cx = cx.clone();
        Box::pin(async move {
            let mut hook = Hook::new();

            let child = component.render(&mut hook);

            Self {
                component,
                child: Box::new(Transition::create(child.clone(), position, &mut cx).await),
                child_element: child,
                hook,
            }
        })
    }

    pub(crate) fn apply(self, parent: &web_sys::Node) -> state::ComponentState {
        let child = self
            .child
            .apply(parent)
            .expect("CreateComponentTransition: child.apply returned no state");

        let position = child.position().cloned();

        state::ComponentState {
            component: self.component,
            child: Box::new(child),
            child_element: self.child_element,
            hook: self.hook,
            position,
        }
    }
}
