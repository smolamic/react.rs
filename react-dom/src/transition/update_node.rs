use crate::{transition::Transition, Hook};
use react_html::HtmlNode;
use std::sync::Arc;

pub(crate) struct UpdateNodeTransition {
    node: Arc<HtmlNode<Hook>>,
    children: Vec<Transition>,
    web_element: web_sys::Element,
    next: Arc<HtmlNode<Hook>>,
}
