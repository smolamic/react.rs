use crate::{
    state::{self, State},
    transition::Transition,
    Context, Hook,
};
use futures::future::join_all;
use react_html::HtmlElement;
use std::{future::Future, pin::Pin};

pub(crate) struct CreateFragmentTransition {
    children: Vec<Transition>,
}

impl CreateFragmentTransition {
    pub(crate) fn from(
        children: Vec<HtmlElement<Hook>>,
        position: Option<web_sys::Node>,
        cx: &mut Context,
    ) -> Pin<Box<Future<Output = Self>>> {
        let cx = cx.clone();
        Box::pin(async move {
            let children: Vec<_> = children
                .into_iter()
                .map(|child| {
                    let mut cx = cx.clone();
                    let position = position.clone();
                    async move { Transition::create(child, position, &mut cx).await }
                })
                .collect();

            Self {
                children: join_all(children).await,
            }
        })
    }

    pub(crate) fn apply(self, parent: &web_sys::Node) -> state::FragmentState {
        let children: Vec<State> = self
            .children
            .into_iter()
            .filter_map(|child| child.apply(parent))
            .collect();

        let position = children.iter().find_map(|child| child.position()).cloned();

        state::FragmentState { children, position }
    }
}
