use crate::{
    state::State,
    transition::{self, Transition},
    tree_node::TreeNode,
};

#[derive(Debug)]
pub(crate) struct RemoveNodeTransition {
    pub(crate) web_element: web_sys::Element,
}

impl RemoveNodeTransition {
    pub(crate) fn from(web_element: web_sys::Element) -> Self {
        Self { web_element }
    }

    pub(crate) fn apply(
        self,
        parent: web_sys::Node,
        children: Vec<TreeNode<Transition>>,
    ) -> (Option<State>, Vec<transition::MapTransitionInput>) {
        debug_assert!(children.is_empty());

        parent
            .remove_child(&self.web_element)
            .expect("Failed to remove node");

        (None, Vec::new())
    }
}
