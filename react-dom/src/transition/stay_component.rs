use crate::tree_node::TreeNode;
use crate::{
    state::{self, State},
    transition::{self, Transition},
};
use react_core::{component::AnyComponent, Hook};
use react_html::{HtmlElement, HtmlNode};
use std::sync::Arc;

#[derive(Debug)]
pub(crate) struct StayComponentTransition {
    pub(crate) component: Arc<dyn AnyComponent<HtmlNode>>,
    pub(crate) rendered_element: HtmlElement,
    pub(crate) hook: Hook,
}

impl StayComponentTransition {
    pub(crate) fn from(
        component: Arc<dyn AnyComponent<HtmlNode>>,
        rendered_element: HtmlElement,
        hook: Hook,
    ) -> Self {
        Self {
            component,
            rendered_element,
            hook,
        }
    }

    pub(crate) fn apply(
        self,
        parent: web_sys::Node,
        children: Vec<TreeNode<Transition>>,
    ) -> (Option<State>, Vec<transition::MapTransitionInput>) {
        let children = children
            .into_iter()
            .map(|node| (node, parent.clone()))
            .collect();

        (
            Some(State::Component(state::ComponentState::from(
                self.component,
                self.rendered_element,
                self.hook,
            ))),
            children,
        )
    }
}
