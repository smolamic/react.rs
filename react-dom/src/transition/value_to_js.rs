use js_sys::{Array, Object, Reflect};
use react_html::Value;
use wasm_bindgen::JsValue;

pub(crate) fn value_to_js(value: &Value) -> JsValue {
    match value {
        Value::String(value) => JsValue::from_str(value),
        Value::Number(value) => JsValue::from_f64(value.clone()),
        Value::Boolean(value) => JsValue::from_bool(value.clone()),
        Value::Object(values) => {
            let js_object = Object::new();

            for (key, value) in values {
                Reflect::set(&js_object, &key[..].into(), &value_to_js(value));
            }

            js_object.into()
        }
        Value::Array(values) => {
            let length = values.len();

            let js_array = Array::new();

            for value in values {
                js_array.push(&value_to_js(value));
            }

            js_array.into()
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use react_html::Value;
    use serde::Deserialize;
    use std::collections::HashMap;
    use wasm_bindgen_test::*;

    #[wasm_bindgen_test]
    fn cast_string() {
        let value: Value = "test".into();

        let js_value = value_to_js(&value);

        assert_eq!(Some("test".to_string()), js_value.as_string());
    }

    #[wasm_bindgen_test]
    fn cast_number() {
        let value: Value = 64.into();

        let js_value = value_to_js(&value);

        assert_eq!(Some(64f64), js_value.as_f64());
    }

    #[wasm_bindgen_test]
    fn cast_boolean() {
        let value: Value = true.into();

        let js_value = value_to_js(&value);

        assert_eq!(Some(true), js_value.as_bool());
    }

    #[wasm_bindgen_test]
    fn cast_object() {
        #[derive(Deserialize, Debug, PartialEq, Clone)]
        struct TestObject {
            field: String,
        }

        impl From<TestObject> for Value {
            fn from(test_object: TestObject) -> Self {
                let mut hash_map = HashMap::new();
                hash_map.insert("field".to_string(), test_object.field.into());

                Value::Object(hash_map)
            }
        }

        let test_object = TestObject {
            field: "test".into(),
        };

        let value: Value = test_object.clone().into();

        let js_value = value_to_js(&value);

        let deserialized: TestObject = js_value.into_serde().unwrap();

        assert_eq!(test_object, deserialized);
    }

    #[wasm_bindgen_test]
    fn cast_array() {
        let vec = vec!["test".to_string()];
        let value: Value = vec.clone().into();

        let js_value = value_to_js(&value);

        let deserialized: Vec<String> = js_value.into_serde().unwrap();

        assert_eq!(vec, deserialized);
    }
}
