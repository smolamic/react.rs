use crate::tree_node::TreeNode;
use crate::{
    state::{self, EventListenerHandle, State},
    transition::{self, Transition},
};
use react_html::HtmlNode;
use std::{collections::HashMap, sync::Arc};
use wasm_bindgen::JsValue;

#[derive(Debug)]
pub(crate) struct StayNodeTransition {
    pub(crate) node: Arc<HtmlNode>,
    pub(crate) web_element: web_sys::Element,
    pub(crate) next: Arc<HtmlNode>,
    pub(crate) event_listener_handles: HashMap<String, state::EventListenerHandle>,
}

impl StayNodeTransition {
    pub(crate) fn apply(
        self,
        children: Vec<TreeNode<Transition>>,
    ) -> (Option<State>, Vec<transition::MapTransitionInput>) {
        let next = self.next;
        let node = self.node;
        let web_element = self.web_element;

        for key in node.attributes().keys() {
            if next.attributes().contains_key(key) {
                continue;
            }
            web_element
                .remove_attribute(key)
                .expect("Failed to remove attribute");
        }

        for (key, value) in next.attributes() {
            web_element
                .set_attribute(key, value)
                .expect("Failed to set attribute");
        }

        for key in node.properties().keys() {
            if next.properties().contains_key(key) {
                continue;
            }
            js_sys::Reflect::set(&web_element, &key[..].into(), &JsValue::NULL);
        }

        for (key, value) in next.properties() {
            js_sys::Reflect::set(
                &web_element,
                &key[..].into(),
                &transition::value_to_js(value),
            );
        }

        let event_listener_handles = self
            .event_listener_handles
            .into_iter()
            .filter_map(|(key, handle)| {
                if let Some(event_handler) = next.event_handlers().get(&key) {
                    if node.event_handlers().get(&key) == Some(event_handler) {
                        Some((key, handle))
                    } else {
                        handle.remove(&web_element, &key);
                        let handle = EventListenerHandle::add(
                            &web_element,
                            &key,
                            event_handler.clone_boxed(),
                        );
                        Some((key, handle))
                    }
                } else {
                    handle.remove(&web_element, &key);
                    None
                }
            })
            .collect();

        let children = children
            .into_iter()
            .map(|node| (node, web_element.clone().into()))
            .collect();

        (
            Some(State::Node(state::NodeState::from(
                next,
                web_element,
                event_listener_handles,
            ))),
            children,
        )
    }
}
