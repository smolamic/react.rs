pub(crate) use crate::{
    error::{log_error, ReactError, UnwrapLog},
    hook::Hook,
    runtime,
};

use std::{fmt, future::Future};

pub(crate) use react_stdweb_ext::webdbg;
pub(crate) use single_threaded_channels::{mpsc, oneshot};

#[derive(Clone, PartialEq)]
pub(crate) struct Context {
    pub(crate) spawner: runtime::Spawner,
    pub(crate) error_sender: mpsc::Sender<ReactError>,
}

impl fmt::Debug for Context {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        fmt.debug_struct("Context").finish()
    }
}

impl Context {
    pub(crate) fn spawn<F>(&self, future: F)
    where
        F: Future<Output = ()> + 'static,
    {
        if let Err(error) = self.spawner.spawn(future) {
            self.send_error(error)
        }
    }

    pub(crate) fn send_error(&self, error: ReactError) {
        log_error(self.error_sender.send(error))
    }
}
