use crate::LocalSpawner;
use react_core::channel;

#[derive(Clone)]
pub(crate) struct Context {
    pub(crate) spawner: LocalSpawner,
    pub(crate) signal_sender: channel::Sender<()>,
}
