use react_core as core;


use std::{
    rc::Rc,
    cell::Cell,
    any::Any,
};

use futures::{
    channel::mpsc,
};

const UPDATE_CHUNK_SIZE: usize = 50;


#[derive(Clone)]
struct AsyncStateSetter<T>(mpsc::Sender<core::hook::StateUpdateObj<T>>) where T: Copy;

impl<T> core::hook::StateSetterImpl<T> for AsyncStateSetter<T> where
    T: Copy,
{
    fn update(&self, update: core::hook::StateUpdateObj<T>) -> Result<(), core::hook::UpdateStateError> {
        self.0.send(update)
    }
}

struct AsyncStateHook<T> where
    T: Copy,
{
    state_setter: AsyncStateSetter<T>,
    next_value: Rc<Cell<T>>,
    current_value: T,
}

trait AnyAsyncStateHook {
    fn update(&mut self);
    fn as_any_mut(&mut self) -> &mut dyn Any;
}

impl<T> AnyAsyncStateHook for AsyncStateHook<T> where
    T: Copy,
{
    fn update(&mut self) {
        self.current_value = self.next_value.get();
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self
    }
}


#[derive(Debug)]
pub enum AsyncHook {
    Initial {
        state_hooks: Vec<Box<dyn AnyAsyncStateHook>>,
        spawner: Spawner,
        signal_sender: mpsc::Sender<()>,
        has_update: Rc<Cell<bool>>,
    },
    Subsequent {
        state_hooks: Vec<Box<dyn AnyAsyncStateHook>>,
        state_index: usize,
        has_update: Rc<Cell<bool>>,
    },
}

impl AsyncHook {
    fn new(ctx: Context) -> Self {
        Self::Initial {
            spawner: ctx.spawner.clone(),
            signal_sender: ctx.signal_sender.clone(),
            state_hooks: Vec::new(),
            has_update: Rc::new(Cell::new(false)),
        }
    }

    fn next(self) {
        match self {
            Self::Initial {
                state_hooks,
                has_update,
            } => {
                Self::Subsequent {
                    state_hooks,
                    state_index: 0,
                    has_update,
                }
            },

            Self::Subsequent {
                state_hooks,
                has_update,
            } => {
                Self::Subsequent {
                    state_hooks,
                    state_index: 0,
                    has_update,
                }
            }
        }
    }
}

struct StateUpdateDaemon<T> where
    T: Copy,
{
    signal_sender: mpsc::Sender<()>,
    has_update: Rc<Cell<bool>>,
    update_receiver: mpsc::Receiver<core::hook::StateUpdateObj<T>>,
    value: Rc<Cell<T>>,
}

impl StateUpdateDaemon {
    async fn run(self) {
        while let Some(updates) = self.update_receiver.chunks(UPDATE_CHUNK_SIZE).next().await {
            self.value.set(
                updates.into_iter().fold(self.value.get(), |value, update| update(value))
            );
            self.has_update.set(true);
            self.signal_sender.send(());
        }
    }
}

impl core::Hook for AsyncHook {
    fn use_state<T>(&mut self, default: T) -> (T, core::StateSetter<T>) where
        T: Copy,
    {
        match self {
            Self::Initial {
                state_hooks,
                spawner,
                signal_sender,
                has_update,
            } => {
                let (update_sender, update_receiver) = mpsc::unbounded();
                let next_value = Rc::new(Cell::new(default));

                spawner.spawn(StateUpdateDaemon {
                    signal_sender: signal_sender.clone(),
                    value: next_value.clone(),
                    has_update: has_update.clone(),
                    update_receiver,
                }.run());

                let state_setter = AsyncStateSetter(update_sender);

                state_hooks.push(Box::new(AsyncStateHook {
                    state_setter: state_setter.clone(),
                    next_value,
                    current_value: default,
                }));

                (
                    default,
                    core::StateSetter::new(Box::new(state_setter)),
                )
            },

            Self::Subsequent {
                state_hooks,
                state_index,
            } => {
                let state_hook: AsyncStateHook<T> = state_hooks.get_mut(state_index)
                    .as_any_mut()
                    .downcast_mut()
                    .unwrap();

                state_index += 1;

                (
                    state_hook.current_value,
                    core::StateSetter::new(Box::new(state_hook.state_setter.clone())),
                )
            },
        }
    }
}
