use react_core::{
    // preamble::*,
    element::{
        AnyComponent,
        ComponentDiff,
    },
};

use crate::{
    vdom,
    hook::Hook,
    core::*,
};

use single_threaded_channels::{
    mpsc,
};

use std::{
    rc::Rc,
    cell::RefCell,
};

use futures::{
    stream,
    stream::StreamExt,
    future::Either,
};


pub(crate) struct Renderer {
    hook: Hook,
    component: Rc<dyn AnyComponent<Hook = Hook>>,
    receiver: mpsc::Receiver<Rc<dyn AnyComponent<Hook = Hook>>>,
    fragment: Rc<RefCell<vdom::ContextFragment>>,
}

impl Renderer {
    pub(crate) fn start(
        ctx: Context,
        component: Rc<dyn AnyComponent<Hook = Hook>>,
        receiver: mpsc::Receiver<Rc<dyn AnyComponent<Hook = Hook>>>,
        fragment: Rc<RefCell<vdom::ContextFragment>>,
    ) {
        webdbg!("Renderer::start");

        let mut hook = Hook::new(ctx);
        {
            let rendered = component.render(&mut hook);
            let mut ctx_fragment = fragment.borrow_mut();
            let children = &mut ctx_fragment.children;
            children.update(&hook.ctx, rendered);
            // TODO add code here to set ctx_fragment.ctx for error handling
        }

        hook.ctx.clone().spawn(Self {
            hook,
            component,
            receiver,
            fragment,
        }.run());
    }

    fn render(&mut self) {
        let rendered = self.component.render(&mut self.hook);
        let mut ctx_fragment = self.fragment.try_borrow_mut().unwrap_log();
        let children = &mut ctx_fragment.children;
        children.update(&self.hook.ctx, rendered);
    }

    async fn run(mut self) {
        while let Some(update) = stream::select(
            self.receiver.by_ref().map(Either::Left),
            self.hook.signal.by_ref().map(Either::Right),
        ).next().await {
            match update {
                Either::Left(next) => {
                    match self.component.compare(next.as_any()) {
                        ComponentDiff::NewType => {
                            Self::start(self.hook.ctx, next, self.receiver, self.fragment);
                            return
                        },
                        ComponentDiff::NewProps => {
                            self.component = next;
                            self.render();
                        },
                        ComponentDiff::Equal => (),
                    }
                },
                Either::Right(_) => {
                    if !self.hook.update() {
                        continue
                    }

                    self.render();
                },
            }
        }
    }
}
