use react_core::{element::AnyComponent, preamble::*};

struct NodeState {
    node: Rc<Node<Hook>>,
    children: Fragment,
    dom_element: web::Element,
}
struct TextState {
    text_node: Rc<TextNode>,
    dom_node: web::TextNode,
}
struct ComponentState {
    sender: mpsc::Sender<Rc<dyn AnyComponent<Hook = Hook>>>,
    fragment: Rc<RefCell<ContextFragment>>,
    dom_node: Option<web::Node>,
}
struct FragmentState {}

enum State {
    Node(NodeState),
    TextNode(TextNodeState),
    Component(ComponentState),
}

impl State {
    fn dom_node(&self) -> Option<web::Node> {
        match self {
            Self::Node(state) => Some(state.dom_element.clone().into()),
            Self::TextNode(state) => Some(state.dom_node.clone().into()),
            Self::Component(state) => state.dom_node.clone(),
        }
    }
}

struct NodeUpdate {
    node: Rc<Node<Hook>>,
    children: Fragment,
}

impl NodeUpdate {
    fn new(ctx: &Context, node: Rc<Node<Hook>>) -> Self {
        let children = Fragment::from(ctx, node.children.clone());
        Self { node, children }
    }
}

struct PropsUpdate {
    node: Rc<Node<Hook>>,
}

impl PropsUpdate {
    fn new(node: Rc<Node<Hook>>) -> Self {
        Self { node }
    }
}

struct TextNodeUpdate {
    text_node: Rc<TextNode>,
}

impl TextNodeUpdate {
    fn new(text_node: Rc<TextNode>) -> Self {
        Self { text_node }
    }
}

struct ComponentUpdate {
    sender: mpsc::Sender<Rc<dyn AnyComponent<Hook = Hook>>>,
    fragment: Rc<RefCell<ContextFragment>>,
}

impl ComponentUpdate {
    fn new(ctx: Context, component: Rc<dyn AnyComponent<Hook = Hook>>) -> Self {
        webdbg!("ComponentUpdate::new");
        let (sender, receiver) = mpsc::channel();
        let fragment = Rc::new(RefCell::new(ContextFragment::new()));

        Renderer::start(ctx, component, receiver, fragment.clone());

        Self { sender, fragment }
    }
}

enum Transform {
    CreateNode(NodeUpdate),
    CreateTextNode(TextNodeUpdate),
    CreateComponent(ComponentUpdate),
    StayNode(NodeState),
    StayTextNode(TextNodeState),
    StayComponent(ComponentState),
    UpdateNode(NodeState, PropsUpdate),
    ReplaceNode(NodeState, NodeUpdate),
    ReplaceNodeWithTextNode(NodeState, TextNodeUpdate),
    ReplaceNodeWithComponent(NodeState, ComponentUpdate),
    ReplaceTextNodeWithNode(TextNodeState, NodeUpdate),
    ReplaceTextNode(TextNodeState, TextNodeUpdate),
    ReplaceTextNodeWithComponent(TextNodeState, ComponentUpdate),
    ReplaceComponentWithNode(ComponentState, NodeUpdate),
    ReplaceComponentWithTextNode(ComponentState, TextNodeUpdate),
    RemoveNode(NodeState),
    RemoveTextNode(TextNodeState),
    RemoveComponent(ComponentState),
}

impl Transform {
    fn dom_node(&self) -> Option<web::Node> {
        match self {
            Self::CreateNode(_) => None,
            Self::CreateTextNode(_) => None,
            Self::CreateComponent(_) => None,
            Self::StayNode(state) => Some(state.dom_element.clone().into()),
            Self::StayTextNode(state) => Some(state.dom_node.clone().into()),
            Self::StayComponent(state) => state.dom_node.clone(),
            Self::UpdateNode(state, _) => Some(state.dom_element.clone().into()),
            Self::ReplaceNode(state, _) => Some(state.dom_element.clone().into()),
            Self::ReplaceNodeWithTextNode(state, _) => Some(state.dom_element.clone().into()),
            Self::ReplaceNodeWithComponent(state, _) => Some(state.dom_element.clone().into()),
            Self::ReplaceTextNodeWithNode(state, _) => Some(state.dom_node.clone().into()),
            Self::ReplaceTextNode(state, _) => Some(state.dom_node.clone().into()),
            Self::ReplaceTextNodeWithComponent(state, _) => Some(state.dom_node.clone().into()),
            Self::ReplaceComponentWithNode(state, _) => state.dom_node.clone(),
            Self::ReplaceComponentWithTextNode(state, _) => state.dom_node.clone(),
            Self::RemoveNode(state) => Some(state.dom_element.clone().into()),
            Self::RemoveTextNode(state) => Some(state.dom_node.clone().into()),
            Self::RemoveComponent(state) => state.dom_node.clone(),
        }
    }
}

impl From<Transform> for Option<State> {
    fn from(transform: Transform) -> Self {
        match transform {
            Transform::CreateNode(_) => None,
            Transform::CreateTextNode(_) => None,
            Transform::CreateComponent(_) => None,
            Transform::StayNode(state) => Some(State::Node(state)),
            Transform::StayTextNode(state) => Some(State::TextNode(state)),
            Transform::StayComponent(state) => Some(State::Component(state)),
            Transform::UpdateNode(state, _) => Some(State::Node(state)),
            Transform::ReplaceNode(state, _) => Some(State::Node(state)),
            Transform::ReplaceNodeWithTextNode(state, _) => Some(State::Node(state)),
            Transform::ReplaceNodeWithComponent(state, _) => Some(State::Node(state)),
            Transform::ReplaceTextNodeWithNode(state, _) => Some(State::TextNode(state)),
            Transform::ReplaceTextNode(state, _) => Some(State::TextNode(state)),
            Transform::ReplaceTextNodeWithComponent(state, _) => Some(State::TextNode(state)),
            Transform::ReplaceComponentWithNode(state, _) => Some(State::Component(state)),
            Transform::ReplaceComponentWithTextNode(state, _) => Some(State::Component(state)),
            Transform::RemoveNode(state) => Some(State::Node(state)),
            Transform::RemoveTextNode(state) => Some(State::TextNode(state)),
            Transform::RemoveComponent(state) => Some(State::Component(state)),
        }
    }
}

pub(crate) struct Fragment(Vec<Transform>);

pub(crate) struct ContextFragment {
    pub(crate) children: Fragment,
    pub(crate) ctx: Option<Context>,
}

impl Fragment {
    fn new() -> Self {
        Self(Vec::new())
    }
    fn from(ctx: &Context, elements: Vec<Element<Hook>>) -> Self {
        let children = elements
            .into_iter()
            .map(|element| match element {
                Element::Node(node) => Transform::CreateNode(NodeUpdate::new(ctx, node)),
                Element::TextNode(text_node) => {
                    Transform::CreateTextNode(TextNodeUpdate::new(text_node))
                }
                Element::Component(component) => {
                    Transform::CreateComponent(ComponentUpdate::new(ctx.clone(), component))
                }
            })
            .collect();

        Self(children)
    }

    pub(crate) fn update(&mut self, ctx: &Context, elements: Vec<Element<Hook>>) {
        let mut states = self
            .0
            .drain(..)
            .map(Into::into)
            .collect::<Vec<Option<State>>>()
            .into_iter();
        let mut elements = elements.into_iter();

        loop {
            match (states.next().and_then(|next| next), elements.next()) {
                (None, Some(Element::Node(node))) => self
                    .0
                    .push(Transform::CreateNode(NodeUpdate::new(ctx, node))),

                (None, Some(Element::TextNode(text_node))) => self
                    .0
                    .push(Transform::CreateTextNode(TextNodeUpdate::new(text_node))),

                (None, Some(Element::Component(component))) => self.0.push(
                    Transform::CreateComponent(ComponentUpdate::new(ctx.clone(), component)),
                ),

                (Some(State::Node(mut state)), Some(Element::Node(node))) => {
                    if state.node.tag_name == node.tag_name {
                        state.children.update(ctx, node.children.clone());

                        self.0
                            .push(Transform::UpdateNode(state, PropsUpdate::new(node)))
                    } else {
                        self.0
                            .push(Transform::ReplaceNode(state, NodeUpdate::new(ctx, node)))
                    }
                }

                (Some(State::Node(state)), Some(Element::TextNode(text_node))) => self.0.push(
                    Transform::ReplaceNodeWithTextNode(state, TextNodeUpdate::new(text_node)),
                ),

                (Some(State::Node(state)), Some(Element::Component(component))) => {
                    self.0.push(Transform::ReplaceNodeWithComponent(
                        state,
                        ComponentUpdate::new(ctx.clone(), component),
                    ))
                }

                (Some(State::TextNode(state)), Some(Element::Node(node))) => self.0.push(
                    Transform::ReplaceTextNodeWithNode(state, NodeUpdate::new(ctx, node)),
                ),

                (Some(State::TextNode(state)), Some(Element::TextNode(text_node))) => self.0.push(
                    Transform::ReplaceTextNode(state, TextNodeUpdate::new(text_node)),
                ),

                (Some(State::TextNode(state)), Some(Element::Component(component))) => {
                    self.0.push(Transform::ReplaceTextNodeWithComponent(
                        state,
                        ComponentUpdate::new(ctx.clone(), component),
                    ))
                }

                (Some(State::Component(state)), Some(Element::Node(node))) => self.0.push(
                    Transform::ReplaceComponentWithNode(state, NodeUpdate::new(ctx, node)),
                ),

                (Some(State::Component(state)), Some(Element::TextNode(text_node))) => self.0.push(
                    Transform::ReplaceComponentWithTextNode(state, TextNodeUpdate::new(text_node)),
                ),

                (Some(State::Component(state)), Some(Element::Component(component))) => {
                    if let Err(error) = state.sender.send(component) {
                        ctx.send_error(error.into());
                    }
                    self.0.push(Transform::StayComponent(state))
                }

                (Some(State::Node(state)), None) => self.0.push(Transform::RemoveNode(state)),

                (Some(State::TextNode(state)), None) => {
                    self.0.push(Transform::RemoveTextNode(state))
                }

                (Some(State::Component(state)), None) => {
                    self.0.push(Transform::RemoveComponent(state))
                }

                (None, None) => {
                    break;
                }
            }
        }
    }

    fn create_node(node: &Node<Hook>) -> Result<web::Element, ReactError> {
        Ok(web::document().create_element(&node.tag_name)?)
    }

    fn commit(
        &mut self,
        ctx: &Context,
        parent: web::Node,
        sibling: Option<web::Node>,
    ) -> Result<Option<web::Node>, ReactError> {
        webdbg!("commit");
        let mut transforms = self
            .0
            .drain(..)
            .collect::<Vec<Transform>>()
            .into_iter()
            .peekable();

        let mut first: Option<web::Node> = None;

        loop {
            let transform = transforms.next();
            let sibling = transforms
                .peek()
                .and_then(Transform::dom_node)
                .or_else(|| sibling.clone());

            match transform {
                Some(Transform::CreateNode(NodeUpdate { node, mut children })) => {
                    let dom_element = web::document().create_element(&node.tag_name)?;
                    for (name, value) in node.attributes.iter() {
                        dom_element.set_attribute(&name, &value)?;
                    }
                    children.commit(ctx, dom_element.clone().into(), None)?;
                    match &sibling {
                        Some(sibling) => {
                            parent
                                .insert_before(&dom_element, sibling)
                                .map_err(|_| ReactError::InsertNodeError)?;
                        }
                        None => {
                            parent.append_child(&dom_element);
                        }
                    }
                    if first.is_none() {
                        first = Some(dom_element.clone().into());
                    }
                    self.0.push(Transform::StayNode(NodeState {
                        node,
                        children,
                        dom_element,
                    }));
                }
                Some(Transform::CreateTextNode(TextNodeUpdate { text_node })) => {
                    let dom_node = web::document().create_text_node(&text_node.text);
                    match &sibling {
                        Some(sibling) => {
                            parent
                                .insert_before(&dom_node, sibling)
                                .map_err(|_| ReactError::InsertNodeError)?;
                        }
                        None => {
                            parent.append_child(&dom_node);
                        }
                    }
                    if first.is_none() {
                        first = Some(dom_node.clone().into());
                    }
                    self.0.push(Transform::StayTextNode(TextNodeState {
                        text_node,
                        dom_node,
                    }))
                }

                Some(Transform::CreateComponent(ComponentUpdate { sender, fragment })) => {
                    let dom_node = {
                        let mut ctx_fragment = fragment.borrow_mut();
                        let local_ctx = ctx_fragment.ctx.take();
                        let children = &mut ctx_fragment.children;

                        let dom_node = children.commit(
                            local_ctx.as_ref().unwrap_or(ctx),
                            parent.clone(),
                            sibling,
                        )?;

                        if first.is_none() {
                            first = dom_node.clone();
                        }

                        ctx_fragment.ctx = local_ctx;
                        dom_node
                    };
                    self.0.push(Transform::StayComponent(ComponentState {
                        sender,
                        fragment,
                        dom_node,
                    }))
                }

                Some(Transform::StayNode(state)) => self.0.push(Transform::StayNode(state)),
                Some(Transform::StayTextNode(state)) => self.0.push(Transform::StayTextNode(state)),
                Some(Transform::StayComponent(state)) => {
                    self.0.push(Transform::StayComponent(state))
                }

                Some(Transform::UpdateNode(
                    NodeState {
                        node: _old_node,
                        children,
                        dom_element,
                    },
                    PropsUpdate { node },
                )) => {
                    // TODO set attributes etc
                    self.0.push(Transform::StayNode(NodeState {
                        node,
                        children,
                        dom_element,
                    }))
                }

                Some(Transform::ReplaceNode(state, NodeUpdate { node, mut children })) => {
                    let dom_element = Self::create_node(&node)?;
                    children.commit(ctx, dom_element.clone().into(), None)?;

                    parent
                        .replace_child(&dom_element, &state.dom_element)
                        .map_err(|_| ReactError::InsertNodeError)?;

                    self.0.push(Transform::StayNode(NodeState {
                        node,
                        children,
                        dom_element,
                    }))
                }

                Some(Transform::ReplaceNodeWithTextNode(state, TextNodeUpdate { text_node })) => {
                    let dom_node = web::document().create_text_node(&text_node.text);

                    parent
                        .replace_child(&dom_node, &state.dom_element)
                        .map_err(|_| ReactError::InsertNodeError)?;

                    self.0.push(Transform::StayTextNode(TextNodeState {
                        text_node,
                        dom_node,
                    }))
                }

                Some(Transform::ReplaceNodeWithComponent(
                    state,
                    ComponentUpdate { sender, fragment },
                )) => {
                    let dom_node = {
                        let mut ctx_fragment = fragment.borrow_mut();
                        let local_ctx = ctx_fragment.ctx.take();
                        let children = &mut ctx_fragment.children;

                        let dom_node = children.commit(
                            local_ctx.as_ref().unwrap_or(ctx),
                            parent.clone(),
                            Some(state.dom_element.clone().into()),
                        )?;

                        parent.remove_child(&state.dom_element)?;

                        ctx_fragment.ctx = local_ctx;
                        dom_node
                    };

                    self.0.push(Transform::StayComponent(ComponentState {
                        sender,
                        fragment,
                        dom_node,
                    }))
                }

                Some(Transform::ReplaceTextNodeWithNode(
                    state,
                    NodeUpdate { node, mut children },
                )) => {
                    let dom_element = Self::create_node(&node)?;
                    children.commit(ctx, dom_element.clone().into(), None)?;

                    parent
                        .replace_child(&dom_element, &state.dom_node)
                        .map_err(|_| ReactError::InsertNodeError)?;

                    self.0.push(Transform::StayNode(NodeState {
                        node,
                        children,
                        dom_element,
                    }))
                }

                Some(Transform::ReplaceTextNode(state, TextNodeUpdate { text_node })) => {
                    let dom_node = web::document().create_text_node(&text_node.text);

                    parent
                        .replace_child(&dom_node, &state.dom_node)
                        .map_err(|_| ReactError::InsertNodeError)?;

                    self.0.push(Transform::StayTextNode(TextNodeState {
                        text_node,
                        dom_node,
                    }))
                }

                Some(Transform::ReplaceTextNodeWithComponent(
                    state,
                    ComponentUpdate { sender, fragment },
                )) => {
                    let dom_node = {
                        let mut ctx_fragment = fragment.borrow_mut();
                        let local_ctx = ctx_fragment.ctx.take();
                        let children = &mut ctx_fragment.children;

                        let dom_node = children.commit(
                            local_ctx.as_ref().unwrap_or(ctx),
                            parent.clone(),
                            Some(state.dom_node.clone().into()),
                        )?;

                        match &dom_node {
                            Some(dom_node) => {
                                parent
                                    .replace_child(dom_node, &state.dom_node)
                                    .map_err(|_| ReactError::InsertNodeError)?;
                            }
                            None => {
                                parent.remove_child(&state.dom_node)?;
                            }
                        }

                        ctx_fragment.ctx = local_ctx;

                        dom_node
                    };

                    self.0.push(Transform::StayComponent(ComponentState {
                        sender,
                        fragment,
                        dom_node,
                    }))
                }

                Some(Transform::ReplaceComponentWithNode(
                    state,
                    NodeUpdate { node, mut children },
                )) => {
                    let dom_element = Self::create_node(&node)?;
                    children.commit(ctx, dom_element.clone().into(), None)?;

                    match state.dom_node {
                        Some(old_dom_node) => {
                            parent
                                .replace_child(&dom_element, &old_dom_node)
                                .map_err(|_| ReactError::InsertNodeError)?;
                        }
                        None => {
                            parent.append_child(&dom_element);
                        }
                    }

                    self.0.push(Transform::StayNode(NodeState {
                        node,
                        children,
                        dom_element,
                    }))
                }

                Some(Transform::ReplaceComponentWithTextNode(
                    state,
                    TextNodeUpdate { text_node },
                )) => {
                    let dom_node = web::document().create_text_node(&text_node.text);

                    match state.dom_node {
                        Some(old_dom_node) => {
                            parent
                                .replace_child(&dom_node, &old_dom_node)
                                .map_err(|_| ReactError::InsertNodeError)?;
                        }
                        None => {
                            parent.append_child(&dom_node);
                        }
                    }

                    self.0.push(Transform::StayTextNode(TextNodeState {
                        text_node,
                        dom_node,
                    }))
                }

                Some(Transform::RemoveNode(state)) => {
                    parent.remove_child(&state.dom_element)?;
                }

                Some(Transform::RemoveTextNode(state)) => {
                    parent.remove_child(&state.dom_node)?;
                }

                Some(Transform::RemoveComponent(_)) => (),

                None => break,
            }
        }
        Ok(first.or(sibling.clone()))
    }
}

impl ContextFragment {
    fn new() -> Self {
        Self {
            children: Fragment::new(),
            ctx: None,
        }
    }
}

pub(crate) struct VDom {
    ctx: Context,
    fragment: RefCell<Fragment>,
    target: web::Node,
}

impl VDom {
    pub(crate) fn new(ctx: Context, element: Element<Hook>, target: web::Node) -> Self {
        webdbg!("VDom::new");
        Self {
            fragment: RefCell::new(Fragment::from(&ctx, vec![element])),
            target,
            ctx,
        }
    }
    pub(crate) fn commit(&self) {
        if let Err(error) = self
            .fragment
            .borrow_mut()
            .commit(&self.ctx, self.target.clone(), None)
        {
            log_error(self.ctx.error_sender.send(error));
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::runtime;
    use std::collections::VecDeque;
    use stdweb::{
        web,
        web::{IElement, IParentNode},
    };

    fn setup_target() -> web::Element {
        let target = web::document().create_element("div").unwrap();
        target.class_list().add("target").unwrap();
        let body = web::document().body().unwrap();
        body.append_child(&target);
        target
    }

    fn setup_context() -> Context {
        let error_sender = mpsc::Sender::new();
        let tasks = Rc::new(RefCell::new(VecDeque::new()));

        Context {
            spawner: runtime::Spawner::new(&tasks),
            error_sender,
        }
    }

    #[test]
    fn test_basic_div() {
        webdbg!("test basic_div");
        let target = setup_target();
        let ctx = setup_context();

        let mut fragment = Fragment::from(&ctx, vec![Node::new("div").into()]);

        fragment.commit(&ctx, target.clone().into(), None).unwrap();

        let div = target.query_selector("div").unwrap();
        assert_ne!(None, div);
    }
}
