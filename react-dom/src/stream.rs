use futures::{future::Either, stream::Stream};
use std::{
    pin::Pin,
    task::{Context, Poll},
};

struct Select<A, B, Sta, Stb>
where
    Sta: Stream<Item = A>,
    Stb: Stream<Item = B>,
{
    a: Sta,
    b: Stb,
}

impl<A, B, Sta, Stb> Stream for Select<A, B, Sta, Stb> {
    type Item = Either<A, B>;

    fn poll_next(self: Pin<&mut Self>, ctx: &mut Context) -> Poll<Option<Self::Item>> {
        match self.a.poll_next(ctx) {
            Poll::Ready(Some(value)) => Poll::Ready(Some(Either::Left(value))),
            Poll::Ready(None) => Poll::Ready(None),
            Poll::Pending => self
                .b
                .poll_next(ctx)
                .map(|next| next.map(|value| Either::Right(value))),
        }
    }
}

pub(crate) fn select<A, B, Sta, Stb>(a: Sta, b: Stb) -> Select<A, B, Sta, Stb>
where
    Sta: Stream<Item = A>,
    Stb: Stream<Item = B>,
{
    Select { a, b }
}
