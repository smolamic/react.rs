#[macro_use]
pub(crate) mod dbg;
pub(crate) mod context;
pub(crate) mod local_spawner;
pub(crate) mod map_tree;
pub(crate) mod state;
pub(crate) mod transition;
pub(crate) mod tree_node;

pub(crate) use context::Context;
use futures::{stream::StreamExt, task::LocalSpawnExt};
use itertools::EitherOrBoth;
pub(crate) use local_spawner::LocalSpawner;
use react_core::channel;
use react_html::HtmlElement;

pub fn render(element: HtmlElement, target: web_sys::Node) {
    let spawner = LocalSpawner;

    let (signal_sender, mut signal_receiver) = channel::bounded(1);

    let cx = Context {
        spawner: spawner.clone(),
        signal_sender,
    };

    let create = state::map_state_to_transition(EitherOrBoth::Right((element.clone(), None)), &cx);


    if let Some(mut state) = transition::map_transition_to_state((create, target.clone())) {
        spawner
            .spawn_local(async move {
                while signal_receiver.next().await.is_some() {
                    let transition = state::map_state_to_transition(
                        EitherOrBoth::Both(state, (element.clone(), None)),
                        &cx,
                    );

                    if let Some(next_state) =
                        transition::map_transition_to_state((transition, target.clone()))
                    {
                        state = next_state;
                    } else {
                        break;
                    }
                }
            })
            .expect("Failed to spawn main event loop");
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;
    use wasm_bindgen::closure::Closure;
    use wasm_bindgen_test::*;
    wasm_bindgen_test_configure!(run_in_browser);
    use react_core::{channel, Component, Hook};
    use react_html::{tag, HtmlElement, HtmlNode};
    use std::sync::Arc;

    pub fn create_element(tag_name: &str) -> web_sys::Element {
        let document = web_sys::window()
            .expect("Failed to get window")
            .document()
            .expect("Failed to get document");
        document.create_element(tag_name).unwrap()
    }

    pub fn create_root() -> web_sys::Element {
        create_element("div")
    }

    pub(crate) fn create_context() -> Context {
        let (signal_sender, _) = channel::bounded(1);

        Context {
            spawner: LocalSpawner,
            signal_sender,
        }
    }

    pub(crate) fn wait_until_idle() -> channel::Receiver<()> {
        let (sender, receiver) = channel::bounded(1);

        let callback: js_sys::Function =
            Closure::once_into_js(move || sender.try_send(()).unwrap()).into();

        web_sys::window()
            .expect("Failed to get window")
            .request_idle_callback(&callback)
            .expect("Failed to request idle callback");

        receiver
    }

    #[wasm_bindgen_test]
    fn tests_can_run() {}

    #[wasm_bindgen_test]
    async fn render_a_div() {
        let root = create_root();
        render(tag::div().build().into(), root.clone().into());
        wait_until_idle().recv().await.unwrap();

        assert_eq!("<div></div>", root.inner_html());
    }

    #[wasm_bindgen_test]
    async fn render_text() {
        let root = create_root();
        render("test".into(), root.clone().into());
        wait_until_idle().recv().await.unwrap();

        assert_eq!("test", root.inner_html());
    }

    #[wasm_bindgen_test]
    async fn render_a_fragment() {
        let root = create_root();
        render(
            vec![tag::div().build().into(), "test".into()].into(),
            root.clone().into(),
        );
        wait_until_idle().recv().await.unwrap();

        assert_eq!("<div></div>test", root.inner_html());
    }

    #[wasm_bindgen_test]
    async fn render_a_component() {
        let root = create_root();

        #[derive(PartialEq, Debug)]
        struct MyComponent;
        impl Component<HtmlNode> for MyComponent {
            fn render(&self, _hook: &mut Hook) -> HtmlElement {
                tag::div().children(vec!["test".into()]).build().into()
            }
        }

        render(
            HtmlElement::Component(Arc::new(MyComponent)),
            root.clone().into(),
        );
        wait_until_idle().recv().await.unwrap();

        assert_eq!("<div>test</div>", root.inner_html());
    }

    #[wasm_bindgen_test]
    async fn render_deeply_nested_divs() {
        let root = create_root();

        let mut element: HtmlElement = "test".into();
        for _ in 0..500 {
            element = tag::div().children(vec![element]).build().into();
        }

        render(element, root.clone().into());

        wait_until_idle().recv().await.unwrap();
        assert!(root.inner_html().starts_with("<div><div><div><div><div>"));
    }
}
