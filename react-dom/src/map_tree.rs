enum UnitOfWork<I, T> {
    Map(I),
    Pop(T),
}

pub(crate) fn map_tree<I, T, O, CI, CO>(root: I, mut callback_in: CI, mut callback_out: CO) -> O
where
    CI: FnMut(I) -> (T, Vec<I>),
    CO: FnMut(T, Vec<O>) -> O,
{
    let mut stack = vec![UnitOfWork::Map(root)];
    let mut buffer = vec![Vec::new()];

    while let Some(unit_of_work) = stack.pop() {
        match unit_of_work {
            UnitOfWork::Pop(value) => {
                let children = buffer.pop().unwrap();

                let output = callback_out(value, children);

                buffer.last_mut().unwrap().push(output);
            }

            UnitOfWork::Map(input) => {
                let (value, children) = callback_in(input);
                stack.push(UnitOfWork::Pop(value));
                buffer.push(Vec::new());

                for child in children.into_iter().rev() {
                    stack.push(UnitOfWork::Map(child));
                }
            }
        }
    }

    return buffer.last_mut().unwrap().pop().unwrap();
}

#[cfg(test)]
mod tests {
    use super::*;

    #[derive(Debug, PartialEq)]
    struct Node {
        value: String,
        children: Vec<Node>,
    }

    impl Node {
        fn new(value: String) -> Self {
            Self {
                value,
                children: Vec::new(),
            }
        }

        fn with_children(value: String, children: Vec<Node>) -> Self {
            Self { value, children }
        }
    }

    fn simple_tree() -> Node {
        Node::with_children(
            "a".to_string(),
            vec![Node::new("b".to_string()), Node::new("c".to_string())],
        )
    }

    #[test]
    fn map_a_simple_tree_width_first() {
        let tree = simple_tree();

        let mapped = map_tree(
            tree,
            |node| (format!("{} mapped", node.value), node.children),
            Node::with_children,
        );

        assert_eq!(
            mapped,
            Node::with_children(
                "a mapped".to_string(),
                vec![
                    Node::new("b mapped".to_string()),
                    Node::new("c mapped".to_string())
                ]
            )
        );
    }

    #[test]
    fn map_a_simple_tree_depth_first() {
        let tree = simple_tree();

        let mapped = map_tree(
            tree,
            |node| (node.value, node.children),
            |value, children| Node {
                value: format!("{} mapped", value),
                children,
            },
        );

        assert_eq!(
            mapped,
            Node::with_children(
                "a mapped".to_string(),
                vec![
                    Node::new("b mapped".to_string()),
                    Node::new("c mapped".to_string())
                ]
            )
        );
    }

    #[test]
    fn pass_parent_and_siblings_to_children() {
        struct Input {
            node: Node,
            parent: Option<String>,
            right_sibling: Option<String>,
        }

        let tree = Node::with_children(
            "a".to_string(),
            vec![
                Node::with_children("b".to_string(), vec![Node::new("c".to_string())]),
                Node::new("d".to_string()),
            ],
        );

        let mapped = map_tree(
            Input {
                node: tree,
                parent: None,
                right_sibling: None,
            },
            |input| {
                let value = format!(
                    "{} p:{:?} r:{:?}",
                    input.node.value, input.parent, input.right_sibling
                );

                let mut children_iter = input.node.children.into_iter().peekable();

                let mut children = Vec::new();

                while let Some(child) = children_iter.next() {
                    let child_input = Input {
                        node: child,
                        parent: Some(input.node.value.clone()),
                        right_sibling: children_iter.peek().map(|node| node.value.clone()),
                    };

                    children.push(child_input);
                }

                (value, children)
            },
            |value, children| Node { value, children },
        );

        assert_eq!(
            mapped,
            Node::with_children(
                "a p:None r:None".to_string(),
                vec![
                    Node::with_children(
                        "b p:Some(\"a\") r:Some(\"d\")".to_string(),
                        vec![Node::new("c p:Some(\"b\") r:None".to_string())]
                    ),
                    Node::new("d p:Some(\"a\") r:None".to_string())
                ]
            )
        );
    }
}
