use crate::core::*;
use react_core::element::AnyComponent;
use react_stdweb_ext::{callback, time};
use std::{cell::BorrowMutError, fmt::Debug, rc::Rc};
use stdweb::{console, web, web::error::IError};

pub trait UnwrapLog {
    type Return;
    type Error;

    fn unwrap_log(self) -> Self::Return;
}

impl<R, E> UnwrapLog for Result<R, E>
where
    E: Debug,
{
    type Return = R;
    type Error = E;

    fn unwrap_log(self) -> R {
        match self {
            Ok(value) => value,
            Err(error) => {
                let msg = format!("Uncaught Error:\n{:?}", error);
                console!(error, &msg);
                panic!(msg);
            }
        }
    }
}

pub(crate) fn log_error<E>(result: Result<(), E>)
where
    E: Debug,
{
    match result {
        Ok(_) => (),
        Err(error) => {
            let msg = format!("Uncaught Error:\n{:?}", error);
            console!(error, msg);
        }
    }
}

#[derive(Debug, Clone)]
pub enum ReactError {
    NodeSenderDisconnected,
    NodeReceiverDisconnected,
    NodeReceiverNotExclusive,
    NodeReceiverPending,
    ComponentSenderDisconnected,
    DomError(String, String),
    InsertNodeError,
    BorrowMutError,
    StateHookInvariantViolation,
    StateHookTypeMismatch,
    EqualityOnHook,
    SetStateOnUnmountedComponent,
    UnknownSpawnError,
    SpawnErrorShutdown,
    TimeConversionError,
    CallbackHandleConversionError,
}

impl From<oneshot::ReceiveError<web::Node>> for ReactError {
    fn from(error: oneshot::ReceiveError<web::Node>) -> Self {
        match error {
            oneshot::ReceiveError::Disconnected(_) => Self::NodeReceiverDisconnected,
        }
    }
}

impl From<oneshot::TryUnwrapError<oneshot::Receiver<web::Node>>> for ReactError {
    fn from(error: oneshot::TryUnwrapError<oneshot::Receiver<web::Node>>) -> Self {
        use oneshot::TryUnwrapError;
        match error {
            TryUnwrapError::Disconnected => Self::NodeReceiverDisconnected,
            TryUnwrapError::NotExclusive(_) => Self::NodeReceiverNotExclusive,
            TryUnwrapError::Pending(_) => Self::NodeReceiverPending,
        }
    }
}

impl From<oneshot::SendError<web::Node>> for ReactError {
    fn from(error: oneshot::SendError<web::Node>) -> Self {
        match error {
            oneshot::SendError::Disconnected(_) => Self::NodeSenderDisconnected,
        }
    }
}

impl From<BorrowMutError> for ReactError {
    fn from(_: BorrowMutError) -> Self {
        Self::BorrowMutError
    }
}

impl From<web::error::InvalidCharacterError> for ReactError {
    fn from(error: web::error::InvalidCharacterError) -> Self {
        Self::DomError(error.name(), error.message())
    }
}

impl From<web::error::NotFoundError> for ReactError {
    fn from(error: web::error::NotFoundError) -> Self {
        Self::DomError(error.name(), error.message())
    }
}

impl From<futures::task::SpawnError> for ReactError {
    fn from(error: futures::task::SpawnError) -> Self {
        if error.is_shutdown() {
            Self::SpawnErrorShutdown
        } else {
            Self::UnknownSpawnError
        }
    }
}

impl From<mpsc::SendError<Rc<dyn AnyComponent<Hook = Hook>>>> for ReactError {
    fn from(error: mpsc::SendError<Rc<dyn AnyComponent<Hook = Hook>>>) -> Self {
        match error {
            mpsc::SendError::Disconnected(_) => ReactError::ComponentSenderDisconnected,
        }
    }
}

impl From<time::ConversionError> for ReactError {
    fn from(_error: time::ConversionError) -> Self {
        Self::TimeConversionError
    }
}

impl From<callback::ConversionError> for ReactError {
    fn from(_error: callback::ConversionError) -> Self {
        Self::CallbackHandleConversionError
    }
}
