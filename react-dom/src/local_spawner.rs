use futures::task::{FutureObj, LocalFutureObj, LocalSpawn, Spawn, SpawnError};

#[derive(Clone)]
pub(crate) struct LocalSpawner;

impl Spawn for LocalSpawner {
    fn spawn_obj(&self, future: FutureObj<'static, ()>) -> Result<(), SpawnError> {
        wasm_bindgen_futures::spawn_local(future);
        Ok(())
    }
}

impl LocalSpawn for LocalSpawner {
    fn spawn_local_obj(&self, future: LocalFutureObj<'static, ()>) -> Result<(), SpawnError> {
        wasm_bindgen_futures::spawn_local(future);
        Ok(())
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;
    use futures::task::{LocalSpawnExt, SpawnExt};
    use wasm_bindgen_test::*;

    #[wasm_bindgen_test]
    async fn test_can_spawn() {
        let spawner = LocalSpawner;

        let value = spawner.spawn_with_handle(async { 32i32 }).unwrap().await;

        assert_eq!(32i32, value);
    }

    #[wasm_bindgen_test]
    async fn test_can_spawn_local() {
        let spawner = LocalSpawner;

        let value = spawner
            .spawn_local_with_handle(async { 32i32 })
            .unwrap()
            .await;

        assert_eq!(32i32, value);
    }
}
