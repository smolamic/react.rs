use react_core::{channel, use_state, Component, Hook};
use react_dom::render;
use react_html::{tag, Callback, HtmlElement, HtmlNode};
use wasm_bindgen::closure::Closure;
use wasm_bindgen_test::*;

wasm_bindgen_test_configure!(run_in_browser);

pub(crate) async fn wait_until_idle() {
    let (sender, receiver) = channel::bounded(1);

    let callback: js_sys::Function =
        Closure::once_into_js(move || sender.try_send(()).unwrap()).into();

    web_sys::window()
        .expect("Failed to get window")
        .request_idle_callback(&callback)
        .expect("Failed to request idle callback");

    receiver.recv().await.unwrap()
}


#[derive(Debug, PartialEq)]
struct Counter;

impl Component<HtmlNode> for Counter {
    fn render(&self, hook: &mut Hook) -> HtmlElement {
        let (count, setter) = use_state(hook, 0);

        tag::div()
            .children(vec![
                tag::p()
                    .id("count")
                    .children(vec![count.to_string().into()])
                    .build()
                    .into(),
                tag::button()
                    .id("button")
                    .on((
                        "click",
                        Callback::new(move |_| setter.update(|count| count + 1)),
                    ))
                    .build()
                    .into(),
            ])
            .build()
            .into()
    }
}

impl From<Counter> for HtmlElement {
    fn from(counter: Counter) -> HtmlElement {
        HtmlElement::from_component(counter)
    }
}

#[wasm_bindgen_test]
async fn count_up() {
    let window = web_sys::window().unwrap();
    let document = window.document().unwrap();
    let root = document.create_element("div").unwrap();

    render(Counter.into(), root.clone().into());

    let count = root.query_selector("#count").unwrap().unwrap();

    assert_eq!("0", count.text_content().unwrap(),);

    let button = root.query_selector("#button").unwrap().unwrap();

    let mut event_init = web_sys::MouseEventInit::new();
    event_init.bubbles(true);

    button
        .dispatch_event(
            &web_sys::MouseEvent::new_with_mouse_event_init_dict("click", &event_init).unwrap(),
        )
        .unwrap();

    wait_until_idle().await;

    assert_eq!("1", count.text_content().unwrap());

    button
        .dispatch_event(
            &web_sys::MouseEvent::new_with_mouse_event_init_dict("click", &event_init).unwrap(),
        )
        .unwrap();

    wait_until_idle().await;

    assert_eq!("2", count.text_content().unwrap());
}
