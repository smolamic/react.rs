use react_core::{channel, Hook};
use react_html::HtmlElement;

pub fn render(element: &HtmlElement) -> String {
    let (signal_sender, _signal_receiver) = channel::bounded(1);

    match element {
        HtmlElement::Component(component) => {
            let mut hook = Hook::new(signal_sender);
            render(&component.render(&mut hook))
        }

        HtmlElement::Node(node) => {
            let attributes: Vec<String> = node
                .attributes()
                .iter()
                .map(|(key, val)| format!("{}=\"{}\"", key, val))
                .collect();
            let children: Vec<String> = node.children().iter().map(|child| render(child)).collect();

            match (attributes.is_empty(), children.is_empty()) {
                (true, true) => format!("<{tag_name} />", tag_name = node.tag_name(),),
                (true, false) => format!(
                    "<{tag_name}>{children}</{tag_name}>",
                    tag_name = node.tag_name(),
                    children = children.join(""),
                ),
                (false, true) => format!(
                    "<{tag_name} {attributes} />",
                    tag_name = node.tag_name(),
                    attributes = attributes.join(" "),
                ),
                (false, false) => format!(
                    "<{tag_name} {attributes}>{children}</{tag_name}>",
                    tag_name = node.tag_name(),
                    attributes = attributes.join(" "),
                    children = children.join(""),
                ),
            }
        }

        HtmlElement::Text(text) => text.as_ref().clone(),

        HtmlElement::Fragment(children) => children
            .iter()
            .map(|child| render(child))
            .collect::<Vec<String>>()
            .join(""),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use react_core::{Component, Hook};
    use react_html::{tag, HtmlNode};
    use std::sync::Arc;
    use typed_builder::TypedBuilder;

    #[derive(TypedBuilder, PartialEq, Debug)]
    struct MyButton {
        #[builder(setter(into))]
        label: String,
    }

    impl Component<HtmlNode> for MyButton {
        fn render(&self, _hook: &mut Hook) -> HtmlElement {
            tag::button()
                .children(vec![self.label.clone().into()])
                .build()
                .into()
        }
    }

    #[test]
    fn render_button() {
        assert_eq!(
            "<button>My Button</button>",
            render(&HtmlElement::Component(Arc::new(
                MyButton::builder().label("My Button").build()
            )))
        );
    }

    #[test]
    fn render_div_button_span() {
        assert_eq!(
            "<div class=\"test\"><button>My Button</button><span>Some Text</span></div>",
            render(
                &tag::div()
                    .attr(("class", "test"))
                    .children(vec![
                        HtmlElement::Component(Arc::new(
                            MyButton::builder().label("My Button").build()
                        )),
                        tag::span()
                            .children(vec!["Some Text".into()])
                            .build()
                            .into(),
                    ])
                    .build()
                    .into()
            )
        );
    }
}
