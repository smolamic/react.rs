use crate::event::{Event, EventTarget};
use std::any::Any;
use std::sync::Arc;

pub trait EventHandler<T = dyn EventTarget, E = dyn Event<T>>: PartialEq + Clone
where
    T: ?Sized,
    E: ?Sized,
{
    fn handle_event(&self, event: E);
}

pub struct Callback<C, T = dyn EventTarget, E = dyn Event<T>>(
    Arc<C>,
    std::marker::PhantomData<T>,
    std::marker::PhantomData<E>,
)
where
    T: ?Sized,
    E: ?Sized,
    C: Fn(&E);

impl<C, T, E> Callback<C, T, E>
where
    C: Fn(&E),
{
    pub fn new(callback: C) -> Self {
        Self(
            Arc::new(callback),
            std::marker::PhantomData,
            std::marker::PhantomData,
        )
    }
}

impl<C, T, E> Clone for Callback<C, T, E>
where
    C: Fn(&E),
{
    fn clone(&self) -> Self {
        Self(
            Arc::clone(&self.0),
            std::marker::PhantomData,
            std::marker::PhantomData,
        )
    }
}

impl<T, E, Lhs, Rhs> PartialEq<Rhs> for Callback<Lhs, T, E>
where
    Lhs: Fn(&E),
{
    fn eq(&self, _other: &Rhs) -> bool {
        false
    }
}

impl<C, T, E> EventHandler<T, E> for Callback<C, T, E>
where
    C: Fn(&E),
{
    fn handle_event(&self, event: &E) {
        self.0(event)
    }
}

pub trait AnyEventHandler<E> {
    fn handle_event(&self, event: E);

    fn as_any(&self) -> &dyn Any;

    fn clone_boxed(&self) -> Box<dyn AnyEventHandler<E>>;

    fn boxed_eq(&self, other: &dyn AnyEventHandler<E>) -> bool;
}

impl<E, H> AnyEventHandler<E> for H
where
    H: EventHandler<E> + 'static,
{
    fn handle_event(&self, event: E) {
        <H as EventHandler<E>>::handle_event(self, event)
    }

    fn as_any(&self) -> &dyn Any {
        self
    }

    fn clone_boxed(&self) -> Box<dyn AnyEventHandler<E>> {
        Box::new(self.clone())
    }

    fn boxed_eq(&self, other: &dyn AnyEventHandler<E>) -> bool {
        if let Some(other) = other.as_any().downcast_ref::<H>() {
            self == other
        } else {
            false
        }
    }
}

impl<E> PartialEq for dyn AnyEventHandler<E> {
    fn eq(&self, other: &dyn AnyEventHandler<E>) -> bool {
        self.boxed_eq(other)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn callbacks_are_unequal() {
        let cb1 = Callback::new(|_: ()| ());
        let cb2 = Callback::new(|_: ()| ());

        assert!(cb1 != cb2);
    }

    #[test]
    fn callbacks_as_any_event_handlers_are_unequal() {
        let cb1: Box<dyn AnyEventHandler<dyn Event>> = Box::new(Callback::new(|_| ()));
        let cb2: Box<dyn AnyEventHandler<dyn Event>> = Box::new(Callback::new(|_| ()));

        assert!(cb1 != cb2);
    }
}
