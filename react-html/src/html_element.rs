use crate::html_node::HtmlNode;
use react_core as core;

pub type HtmlElement = core::Element<HtmlNode>;
