macro_rules! make_tag {
    ($tag_name:ident) => {
        pub fn $tag_name() -> TaggedHtmlNodeBuilder {
            HtmlNode::builder().tag_name(stringify!($tag_name))
        }
    };
}

macro_rules! make_tag_extended {
    ($tag_name:ident, $builder_name:ident) => {
        pub struct $builder_name(TaggedHtmlNodeBuilder);

        impl $builder_name {
            pub fn attr<K, V>(self, attr: (K, V)) -> Self
            where
                K: Into<String>,
                V: Into<String>,
            {
                Self(self.0.attr(attr))
            }

            pub fn prop<K, V>(self, prop: (K, V)) -> Self
            where
                K: Into<String>,
                V: Into<Value>,
            {
                Self(self.0.prop(prop))
            }

            pub fn id<T>(self, id: T) -> Self
            where
                T: Into<String>,
            {
                Self(self.0.id(id))
            }

            pub fn class_name<T>(self, class_name: T) -> Self
            where
                T: Into<String>,
            {
                Self(self.0.class_name(class_name))
            }

            pub fn on<T, H>(self, on: (T, H)) -> Self
            where
                T: Into<String>,
                H: EventHandler + 'static,
            {
                Self(self.0.on(on))
            }

            pub fn on_click<H>(self, event_handler: H) -> Self
            where
                H: EventHandler + 'static,
            {
                Self(self.0.on_click(event_handler))
            }

            pub fn build(self) -> HtmlNode {
                self.0.build()
            }
        }

        pub fn $tag_name() -> $builder_name {
            $builder_name(HtmlNode::builder().tag_name(stringify!($tag_name)))
        }
    };
}
