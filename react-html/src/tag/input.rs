use crate::{event_handler::EventHandler, html_node::TaggedHtmlNodeBuilder, HtmlNode, Value};

make_tag_extended!(input, InputBuilder);

impl InputBuilder {
    pub fn type_<T>(self, type_: T) -> Self
    where
        T: Into<String>,
    {
        Self(self.0.prop(("type", type_.into())))
    }

    pub fn on_change<H>(self, event_handler: H) -> Self
    where
        H: EventHandler + 'static,
    {
        Self(self.0.on(("change", event_handler)))
    }

    pub fn value<T>(self, value: T) -> Self
    where
        T: Into<Value>,
    {
        Self(self.0.prop(("value", value)))
    }
}
