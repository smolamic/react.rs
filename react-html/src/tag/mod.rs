#[macro_use]
mod macros;
mod input;

use crate::html_node::{HtmlNode, TaggedHtmlNodeBuilder};
pub use input::input;

make_tag!(div);
make_tag!(span);
make_tag!(button);
make_tag!(p);
make_tag!(li);
make_tag!(ul);

#[cfg(test)]
mod tests {
    use super::*;
    use crate::HtmlNode;

    #[test]
    fn can_build_basic_tags() {
        let my_div = div().build();

        assert_eq!(HtmlNode::new("div"), my_div);
    }
}
