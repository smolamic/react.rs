pub trait EventTarget {}

pub trait Event<T: ?Sized = dyn EventTarget> {
    fn target(&self) -> &T;
}
