use std::collections::HashMap;

#[derive(PartialEq, Debug)]
pub enum Value {
    String(String),
    Number(f64),
    Boolean(bool),
    Object(HashMap<String, Value>),
    Array(Vec<Value>),
}

impl From<String> for Value {
    fn from(string: String) -> Self {
        Self::String(string)
    }
}

impl From<&str> for Value {
    fn from(string: &str) -> Self {
        Self::String(string.to_string())
    }
}

impl From<f64> for Value {
    fn from(number: f64) -> Self {
        Self::Number(number)
    }
}

impl From<f32> for Value {
    fn from(number: f32) -> Self {
        Self::Number(number.into())
    }
}

impl From<u32> for Value {
    fn from(number: u32) -> Self {
        Self::Number(number.into())
    }
}

impl From<i32> for Value {
    fn from(number: i32) -> Self {
        Self::Number(number.into())
    }
}

impl From<bool> for Value {
    fn from(boolean: bool) -> Self {
        Self::Boolean(boolean)
    }
}

impl<T> From<Vec<T>> for Value
where
    T: Into<Value>,
{
    fn from(array: Vec<T>) -> Self {
        Self::Array(array.into_iter().map(Into::into).collect())
    }
}

impl<T> From<HashMap<String, T>> for Value
where
    T: Into<Value>,
{
    fn from(object: HashMap<String, T>) -> Self {
        Self::Object(
            object
                .into_iter()
                .map(|(key, value)| (key, value.into()))
                .collect(),
        )
    }
}
