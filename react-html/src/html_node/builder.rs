use crate::{
    event::Event,
    event_handler::{AnyEventHandler, EventHandler},
    HtmlElement, HtmlNode, Value,
};
use std::collections::HashMap;

pub struct UntaggedHtmlNodeBuilder {
    attributes_: HashMap<String, String>,
    properties_: HashMap<String, Value>,
    children_: Vec<HtmlElement>,
    event_handlers_: HashMap<String, Box<dyn AnyEventHandler<dyn Event>>>,
}

impl UntaggedHtmlNodeBuilder {
    pub(crate) fn new() -> Self {
        Self {
            attributes_: HashMap::new(),
            properties_: HashMap::new(),
            children_: Vec::new(),
            event_handlers_: HashMap::new(),
        }
    }

    pub fn tag_name<T>(self, tag_name: T) -> TaggedHtmlNodeBuilder
    where
        T: Into<String>,
    {
        TaggedHtmlNodeBuilder {
            tag_name_: tag_name.into(),
            base: self,
        }
    }

    pub fn attr<K, V>(mut self, (key, value): (K, V)) -> Self
    where
        K: Into<String>,
        V: Into<String>,
    {
        self.attributes_.insert(key.into(), value.into());
        self
    }

    pub fn prop<K, V>(mut self, (key, value): (K, V)) -> Self
    where
        K: Into<String>,
        V: Into<Value>,
    {
        self.properties_.insert(key.into(), value.into());
        self
    }

    pub fn id<T>(self, id: T) -> Self
    where
        T: Into<String>,
    {
        self.prop(("id", Value::String(id.into())))
    }

    pub fn class_name<T>(self, class_name: T) -> Self
    where
        T: Into<String>,
    {
        self.prop(("className", Value::String(class_name.into())))
    }

    pub fn children(mut self, children: Vec<HtmlElement>) -> Self {
        self.children_ = children;
        self
    }

    pub fn on<T, H>(mut self, (type_, event_handler): (T, H)) -> Self
    where
        T: Into<String>,
        H: EventHandler + 'static,
    {
        self.event_handlers_
            .insert(type_.into(), Box::new(event_handler));
        self
    }

    pub fn on_click<H>(self, event_handler: H) -> Self
    where
        H: EventHandler + 'static,
    {
        self.on(("click", event_handler))
    }
}

pub struct TaggedHtmlNodeBuilder {
    tag_name_: String,
    base: UntaggedHtmlNodeBuilder,
}

impl TaggedHtmlNodeBuilder {
    pub fn tag_name<T>(mut self, tag_name: T) -> Self
    where
        T: Into<String>,
    {
        self.tag_name_ = tag_name.into();
        self
    }

    pub fn attr<K, V>(mut self, (key, value): (K, V)) -> Self
    where
        K: Into<String>,
        V: Into<String>,
    {
        self.base.attributes_.insert(key.into(), value.into());
        self
    }

    pub fn prop<K, V>(mut self, (key, value): (K, V)) -> Self
    where
        K: Into<String>,
        V: Into<Value>,
    {
        self.base.properties_.insert(key.into(), value.into());
        self
    }

    pub fn id<T>(self, id: T) -> Self
    where
        T: Into<String>,
    {
        self.prop(("id", Value::String(id.into())))
    }

    pub fn class_name<T>(self, class_name: T) -> Self
    where
        T: Into<String>,
    {
        self.prop(("className", Value::String(class_name.into())))
    }

    pub fn children(mut self, children: Vec<HtmlElement>) -> Self {
        self.base.children_ = children;
        self
    }

    pub fn on<T, H>(mut self, (type_, event_handler): (T, H)) -> Self
    where
        T: Into<String>,
        H: EventHandler + 'static,
    {
        self.base
            .event_handlers_
            .insert(type_.into(), Box::new(event_handler));
        self
    }

    pub fn on_click<H>(self, event_handler: H) -> Self
    where
        H: EventHandler + 'static,
    {
        self.on(("click", event_handler))
    }

    pub fn build(self) -> HtmlNode {
        HtmlNode {
            tag_name_: self.tag_name_,
            attributes_: self.base.attributes_,
            properties_: self.base.properties_,
            children_: self.base.children_,
            event_handlers_: self.base.event_handlers_,
        }
    }
}
