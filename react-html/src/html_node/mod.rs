mod builder;

use crate::{event::Event, event_handler::AnyEventHandler, HtmlElement, Value};
pub use builder::{TaggedHtmlNodeBuilder, UntaggedHtmlNodeBuilder};
use react_core as core;
use std::{collections::HashMap, fmt};

#[derive(PartialEq)]
pub struct HtmlNode {
    pub(crate) tag_name_: String,
    pub(crate) attributes_: HashMap<String, String>,
    pub(crate) properties_: HashMap<String, Value>,
    pub(crate) children_: Vec<HtmlElement>,
    pub(crate) event_handlers_: HashMap<String, Box<dyn AnyEventHandler<dyn Event>>>,
}

impl core::Node for HtmlNode {}

impl fmt::Debug for HtmlNode {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        //let mut events = self.events.iter()
        //    .map(|(key, _)| format!("{}: <EventHandler>", key))
        //    .fold("".to_string(), |out, next| {
        //        out.push_str(&next);
        //        out
        //    });
        fmt.debug_struct("Node")
            .field("tag_name", &self.tag_name_)
            .field("attributes", &self.attributes_)
            .field("properties", &self.properties_)
            .field("children", &self.children_)
            .finish()
    }
}

impl HtmlNode {
    pub fn new<N>(tag_name: N) -> Self
    where
        N: Into<String>,
    {
        Self {
            tag_name_: tag_name.into(),
            attributes_: HashMap::new(),
            properties_: HashMap::new(),
            children_: Vec::new(),
            event_handlers_: HashMap::new(),
        }
    }

    pub fn builder() -> UntaggedHtmlNodeBuilder {
        UntaggedHtmlNodeBuilder::new()
    }

    pub fn tag_name(&self) -> &str {
        &self.tag_name_[..]
    }

    pub fn attributes(&self) -> &HashMap<String, String> {
        &self.attributes_
    }

    pub fn properties(&self) -> &HashMap<String, Value> {
        &self.properties_
    }

    pub fn children(&self) -> &Vec<HtmlElement> {
        &self.children_
    }

    pub fn event_handlers(&self) -> &HashMap<String, Box<dyn AnyEventHandler<dyn Event>>> {
        &self.event_handlers_
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{
        event::Event,
        event_handler::{Callback, EventHandler},
        HtmlElement,
    };
    use react_core as core;
    use std::sync::Arc;

    #[test]
    fn node_to_element() {
        let element: HtmlElement = HtmlNode::new("div").into();
        assert_eq!(
            element,
            core::Element::<HtmlNode>::Node(Arc::new(HtmlNode::new("div")))
        );
    }

    #[test]
    fn node_with_callback_event_handler() {
        HtmlNode::builder()
            .on((
                "click",
                Callback::new(|_| {
                    println!("test");
                }),
            ))
            .tag_name("button")
            .build();
    }

    #[test]
    fn node_with_struct_event_handler() {
        #[derive(PartialEq, Clone)]
        struct MyEventHandler;

        impl EventHandler for MyEventHandler {
            fn handle_event(&self, _: &dyn Event) {
                println!("test")
            }
        }

        HtmlNode::builder()
            .on(("click", MyEventHandler))
            .tag_name("button")
            .build();
    }
}
