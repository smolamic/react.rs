use crate::channel::{self, Receiver, Sender, SenderExt};
use futures::{
    task::{Spawn, SpawnExt},
    stream::StreamExt,
};
use std::{sync::Arc, any::Any};

pub trait Model: PartialEq + Default {
    type Msg: Send + Clone;

    fn update(self: Arc<Self>, msg: Self::Msg) -> Arc<Self>;
}

#[derive(Clone)]
pub struct Dispatcher<Msg>
where
    Msg: Clone,
{
    msg_sender: Sender<Msg>,
}

impl<Msg> Dispatcher<Msg>
where
    Msg: Clone,
{
    fn new(msg_sender: Sender<Msg>) -> Self {
        Self {
            msg_sender,
        }
    }

    pub fn dispatch(&self, msg: Msg) {
        self.msg_sender
            .try_send(msg)
            .expect("Failed to send message");

    }
}

pub(crate) struct ModelHook<M>
where
    M: Model,
{
    dispatcher: Dispatcher<<M as Model>::Msg>,
    receiver: Receiver<Arc<M>>,
    value: Arc<M>,
}

impl<M> ModelHook<M>
where
    M: Model + Send + Sync +  'static,
{
    pub(crate) fn new<S>(spawner: &S, signal_sender: Sender<()>) -> Self
    where
        S: Spawn,
    {
        let (msg_sender, msg_receiver) = channel::unbounded();
        let (value_sender, value_receiver) = channel::bounded(1);

        let value = Arc::new(M::default());


        spawner
            .spawn(Self::update(value.clone(), msg_receiver, value_sender, signal_sender))
            .expect("Failed to spawn Model Updater");

        Self {
            dispatcher: Dispatcher::new(msg_sender),
            receiver: value_receiver,
            value,
        }
    }

    async fn update(
        mut value: Arc<M>,
        msg_receiver: Receiver<<M as Model>::Msg>,
        value_sender: Sender<Arc<M>>,
        signal_sender: Sender<()>,
    ) {
        while let Ok(msg) = msg_receiver.recv().await {
            let new_value = <M as Model>::update(Arc::clone(&value), msg);

            if Arc::ptr_eq(&value, &new_value) {
                continue
            }

            value = new_value.clone();
            let _ = value_sender.send(new_value).await;
            signal_sender
                .send_or_drop(())
                .expect("Failed to trigger re-render");
        }
    }

    pub(crate) fn use_model(&self) -> (Arc<M>, Dispatcher<<M as Model>::Msg>) {
        (self.value.clone(), self.dispatcher.clone())
    }
}

pub(crate) trait AnyModelHook {
    fn update(&mut self) -> bool;

    fn as_any(&self) -> &dyn Any;
}

impl<M> AnyModelHook for ModelHook<M>
where
    M: Model + Send + Sync + 'static,
{
    fn update(&mut self) -> bool {
        let mut value = None;

        while let Ok(next_value) = self.receiver.try_recv() {
            value = Some(next_value);
        }

        if let Some(value) = value {
            if value != self.value {
                self.value = value;
                return true
            }
        }

        false
    }

    fn as_any(&self) -> &dyn Any {
        self
    }
}

impl Model for String {
    type Msg = String;

    fn update(self: Arc<Self>, msg: Self::Msg) -> Arc<Self> {
        Arc::new(msg)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::channel;
    use futures::executor::LocalPool;

    #[derive(PartialEq, Default, Clone, Debug)]
    struct CounterModel {
        count: u8,
    }

    #[derive(Clone)]
    enum CounterMsg {
        Increase
    }

    impl Model for CounterModel {
        type Msg = CounterMsg;

        fn update(mut self: Arc<Self>, msg: Self::Msg) -> Arc<Self> {
            println!("update");
            match msg {
                CounterMsg::Increase => {
                    Arc::make_mut(&mut self).count += 1;
                }
            }

            self
        }
    }

    #[test]
    fn create_dispatcher() {
        let (msg_sender, _) = channel::unbounded();

        let _dispatcher = Dispatcher::<()>::new(msg_sender);
    }

    #[test]
    fn create_model_hook() {
        #[derive(PartialEq, Default)]
        struct TestModel;

        impl Model for TestModel {
            type Msg = ();

            fn update(self: Arc<Self>, _msg: Self::Msg) -> Arc<Self> {
                self
            }
        }

        let (signal_sender, _) = channel::bounded(1);
        let pool = LocalPool::new();
        let _model_hook = ModelHook::<TestModel>::new(&pool.spawner(), signal_sender);
    }

    #[test]
    fn run_async_update() {
        let mut pool = LocalPool::new();

        let value = Arc::new(CounterModel::default());

        let (msg_sender, msg_receiver) = channel::unbounded();
        let (value_sender, value_receiver) = channel::unbounded();
        let (signal_sender, signal_receiver) = channel::unbounded();

        pool.spawner().spawn(ModelHook::update(value, msg_receiver, value_sender, signal_sender)).unwrap();

        msg_sender.try_send(CounterMsg::Increase).unwrap();

        pool.run_until_stalled();

        assert_eq!(1, signal_receiver.len());
        assert_eq!(1, value_receiver.len());
        let value = value_receiver.try_recv().unwrap();
        assert_eq!(1, value.count);
    }

    #[test]
    fn count_to_2() {

        let mut pool = LocalPool::new();
        let (signal_sender, _signal_receiver) = channel::bounded(1);

        let mut model_hook = ModelHook::<CounterModel>::new(&pool.spawner(), signal_sender);

        let (model, dispatcher) = model_hook.use_model();

        assert_eq!(0, model.count);

        dispatcher.dispatch(CounterMsg::Increase);
        dispatcher.dispatch(CounterMsg::Increase);

        pool.run_until_stalled();

        assert_eq!(true, model_hook.update());

        let (model, _) = model_hook.use_model();

        assert_eq!(2, model.count);
    }

    #[test]
    fn use_a_string() {
        let mut pool = LocalPool::new();
        let (signal_sender, _signal_receiver) = channel::bounded(1);

        let mut model_hook = ModelHook::<String>::new(&pool.spawner(), signal_sender);

        let (model, dispatcher) = model_hook.use_model();

        assert_eq!(Arc::new("".to_string()), model);

        dispatcher.dispatch("test".to_string());

        pool.run_until_stalled();
        assert_eq!(true, model_hook.update());

        let (model, _) = model_hook.use_model();

        assert_eq!(Arc::new("test".to_string()), model);
    }
}
