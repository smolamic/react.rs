mod model;
mod state;

use async_channel::Sender;
use futures::task::Spawn;
pub use model::Model;
use model::{AnyModelHook, Dispatcher, ModelHook};
pub use state::StateSetter;
use state::{AnyStateHook, StateHook};
use std::sync::Arc;

enum HookState {
    Initial {
        state_hooks: Vec<Box<dyn AnyStateHook>>,
        model_hooks: Vec<Box<dyn AnyModelHook>>,
        signal_sender: Sender<()>,
        spawner: Box<dyn Spawn>,
    },
    Subsequent {
        state_hooks: Vec<Box<dyn AnyStateHook>>,
        state_index: usize,
        model_hooks: Vec<Box<dyn AnyModelHook>>,
        model_index: usize,
    },
}

impl std::fmt::Debug for HookState {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Initial { .. } => f.write_str("HookState::Initial"),
            Self::Subsequent { .. } => f.write_str("HookState::Subsequent"),
        }
    }
}

#[derive(Debug)]
pub struct Hook {
    state: HookState,
}

impl Hook {
    pub fn new<S>(spawner: S, signal_sender: Sender<()>) -> Self
    where
        S: Spawn + 'static,
    {
        Self {
            state: HookState::Initial {
                state_hooks: Vec::new(),
                model_hooks: Vec::new(),
                signal_sender,
                spawner: Box::new(spawner),
            },
        }
    }

    fn use_state<T>(&mut self, default: T) -> (T, StateSetter<T>)
    where
        T: Copy + 'static + PartialEq + Send,
    {
        match &mut self.state {
            HookState::Initial {
                state_hooks,
                signal_sender,
                ..
            } => {
                let state_hook = StateHook::new(signal_sender.clone(), default);

                let state = state_hook.use_state();

                state_hooks.push(Box::new(state_hook));

                state
            }
            HookState::Subsequent {
                state_hooks,
                state_index,
                ..
            } => {
                let any_state_hook = state_hooks
                    .get(state_index.to_owned())
                    .expect("Invalid Hook Call");

                let state_hook: &StateHook<T> = any_state_hook
                    .as_any()
                    .downcast_ref()
                    .expect("Invalid Hook Call");

                state_hook.use_state()
            }
        }
    }

    fn use_model<M>(&mut self) -> (Arc<M>, Dispatcher<<M as Model>::Msg>)
    where
        M: Model + Send + Sync + 'static,
    {
        match &mut self.state {
            HookState::Initial {
                model_hooks,
                signal_sender,
                spawner,
                ..
            } => {
                let model_hook = ModelHook::new(spawner, signal_sender.clone());

                let model = model_hook.use_model();

                model_hooks.push(Box::new(model_hook));

                model
            }

            HookState::Subsequent {
                model_hooks,
                model_index,
                ..
            } => {
                let any_model_hook = model_hooks
                    .get(model_index.to_owned())
                    .expect("Invalid Hook Call");

                let model_hook: &ModelHook<M> = any_model_hook
                    .as_any()
                    .downcast_ref()
                    .expect("Invalid Hook Call");

                model_hook.use_model()
            }
        }
    }

    pub fn update(&mut self) -> bool {
        let mut has_changed = false;

        match self.state {
            HookState::Initial {
                ref mut state_hooks,
                ref mut model_hooks,
                ..
            } => {
                let mut state_hooks = std::mem::replace(state_hooks, Vec::new());
                has_changed =
                    has_changed || state_hooks.iter_mut().any(|state_hook| state_hook.update());

                let mut model_hooks = std::mem::replace(model_hooks, Vec::new());
                has_changed =
                    has_changed || model_hooks.iter_mut().any(|model_hook| model_hook.update());

                self.state = HookState::Subsequent {
                    state_hooks,
                    state_index: 0,
                    model_hooks,
                    model_index: 0,
                }
            }

            HookState::Subsequent {
                ref mut state_hooks,
                ref mut state_index,
                ref mut model_hooks,
                ref mut model_index,
            } => {
                has_changed =
                    has_changed || state_hooks.iter_mut().any(|state_hook| state_hook.update());
                *state_index = 0;

                has_changed =
                    has_changed || model_hooks.iter_mut().any(|model_hook| model_hook.update());
                *model_index = 0;
            }
        }

        has_changed
    }
}

pub fn use_state<T>(hook: &mut Hook, default: T) -> (T, StateSetter<T>)
where
    T: Copy + 'static + PartialEq + Send,
{
    hook.use_state(default)
}

pub fn use_model<M>(hook: &mut Hook) -> (Arc<M>, Dispatcher<<M as Model>::Msg>)
where
    M: Model + Send + Sync + 'static,
{
    hook.use_model()
}

#[cfg(test)]
pub(crate) mod tests {
    use super::*;
    use futures::executor::LocalPool;

    #[test]
    fn create_hook() {
        let (signal_sender, _) = async_channel::bounded(1);
        let pool = LocalPool::new();
        let _hook = Hook::new(pool.spawner(), signal_sender);
    }

    #[test]
    fn use_some_state() {
        let (signal_sender, _signal_receiver) = async_channel::bounded(1);
        let pool = LocalPool::new();
        let mut hook = Hook::new(pool.spawner(), signal_sender);

        let (state, state_setter) = use_state(&mut hook, 3);

        assert_eq!(3, state);

        state_setter.update(|count| count + 1);

        let has_changed = hook.update();

        assert_eq!(has_changed, true);

        let (state, _) = use_state(&mut hook, 3);

        assert_eq!(4, state);
    }

    #[test]
    fn use_some_model() {
        let (signal_sender, _signal_receiver) = async_channel::bounded(1);
        let pool = LocalPool::new();
        let mut hook = Hook::new(pool.spawner(), signal_sender);

        #[derive(PartialEq, Default, Debug)]
        struct SomeModel;

        impl Model for SomeModel {
            type Msg = ();

            fn update(self: Arc<Self>, _: Self::Msg) -> Arc<Self> {
                self
            }
        }

        let (value, _dispatcher) = use_model::<SomeModel>(&mut hook);

        assert_eq!(Arc::new(SomeModel), value);
    }
}
