use crate::channel::{self, Receiver, Sender, SenderExt};
use std::any::Any;

pub type StateUpdateObj<T> = Box<dyn FnOnce(T) -> T + 'static + Send>;

#[derive(Clone)]
pub struct StateSetter<T>
where
    T: Copy + Send,
{
    update_sender: Sender<StateUpdateObj<T>>,
    signal_sender: Sender<()>,
}

impl<T> StateSetter<T>
where
    T: Copy + Send,
{
    fn new(update_sender: Sender<StateUpdateObj<T>>, signal_sender: Sender<()>) -> Self {
        Self {
            update_sender,
            signal_sender,
        }
    }

    pub fn update<F>(&self, update: F)
    where
        F: FnOnce(T) -> T + 'static + Send,
    {
        self.update_sender
            .try_send(Box::new(update))
            .expect("Failed to send update");

        self.signal_sender
            .send_or_drop(())
            .expect("Failed to trigger re-render");
    }
}

pub(crate) struct StateHook<T>
where
    T: Copy + Send,
{
    state_setter: StateSetter<T>,
    receiver: Receiver<StateUpdateObj<T>>,
    value: T,
}

impl<T> StateHook<T>
where
    T: Copy + Send,
{
    pub(crate) fn new(signal_sender: Sender<()>, value: T) -> Self {
        let (update_sender, update_receiver) = channel::unbounded();
        Self {
            state_setter: StateSetter::new(update_sender, signal_sender),
            receiver: update_receiver,
            value,
        }
    }

    pub(crate) fn use_state(&self) -> (T, StateSetter<T>) {
        (self.value, self.state_setter.clone())
    }
}

pub(crate) trait AnyStateHook: Send {
    fn update(&mut self) -> bool;
    fn as_any(&self) -> &dyn Any;
}

impl<T> AnyStateHook for StateHook<T>
where
    T: Copy + 'static + PartialEq + Send,
{
    fn update(&mut self) -> bool {
        let mut next_value = self.value;

        while let Ok(update) = self.receiver.try_recv() {
            next_value = update(self.value);
        }

        if next_value != self.value {
            self.value = next_value;
            return true;
        }

        false
    }

    fn as_any(&self) -> &dyn Any {
        self
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::channel;

    #[test]
    fn create_state_setter() {
        let (update_sender, _) = channel::unbounded();
        let (signal_sender, _) = channel::bounded(1);
        let _state_setter = StateSetter::<()>::new(update_sender, signal_sender);
    }

    #[test]
    fn create_state_hook() {
        let (signal_sender, _) = channel::bounded(1);
        let _state_hook = StateHook::new(signal_sender, 1);
    }

    #[test]
    fn count_to_2() {
        let (signal_sender, _signal_receiver) = channel::bounded(1);
        let mut state_hook = StateHook::new(signal_sender, 1);

        let (count, count_setter) = state_hook.use_state();

        assert_eq!(count, 1);

        count_setter.update(|count| count + 1);

        state_hook.update();

        let (count, _) = state_hook.use_state();

        assert_eq!(count, 2);
    }
}
