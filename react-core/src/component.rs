use crate::{element::Element, hook::Hook, node::Node};

use std::{any::Any, fmt};

#[derive(PartialEq, Debug)]
pub enum ComponentDiff {
    NewType,
    NewProps,
    Equal,
}

pub trait Component<N>: PartialEq + fmt::Debug
where
    N: Node,
{
    fn render(&self, hook: &mut Hook) -> Element<N>;
}

pub trait AnyComponent<N>: fmt::Debug
where
    N: Node,
{
    fn compare(&self, other: &dyn Any) -> ComponentDiff;
    fn as_any(&self) -> &dyn Any;
    fn render(&self, hook: &mut Hook) -> Element<N>;
}

impl<N, C> AnyComponent<N> for C
where
    N: Node,
    C: Component<N> + 'static,
    Self: Sized,
{
    fn compare(&self, other: &dyn Any) -> ComponentDiff {
        let this = self;
        if let Some(other) = other.downcast_ref::<C>() {
            if this == other {
                return ComponentDiff::Equal;
            } else {
                return ComponentDiff::NewProps;
            }
        }
        ComponentDiff::NewType
    }

    fn as_any(&self) -> &dyn Any {
        self
    }

    fn render(&self, hook: &mut Hook) -> Element<N> {
        self.render(hook)
    }
}

impl<N> PartialEq for &(dyn AnyComponent<N> + 'static)
where
    N: Node,
{
    fn eq(&self, other: &Self) -> bool {
        self.compare(other.as_any()) == ComponentDiff::Equal
    }
}

pub mod fakes {
    use super::*;

    #[derive(PartialEq, Debug)]
    pub(crate) struct Null;

    impl<N> Component<N> for Null
    where
        N: Node,
    {
        fn render(&self, _hook: &mut Hook) -> Element<N> {
            Vec::new().into()
        }
    }
}

#[cfg(test)]
pub(crate) mod tests {
    use super::*;
    use crate::{channel, element::Element, node::fakes::FakeNode};
    use futures::executor::LocalPool;

    #[test]
    fn render_component() {
        let null: fakes::Null = fakes::Null;
        let (signal_sender, _) = channel::bounded(1);
        let pool = LocalPool::new();
        let mut hook = Hook::new(pool.spawner(), signal_sender);

        assert_eq!(
            Component::<FakeNode>::render(&null, &mut hook),
            Element::Fragment(Vec::new())
        );

        let dyn_null: &dyn AnyComponent<FakeNode> = &null;
        assert_eq!(
            AnyComponent::render(dyn_null, &mut hook),
            Element::Fragment(Vec::new())
        );
    }

    #[test]
    fn equal_components() {
        let a = fakes::Null;
        let b = fakes::Null;

        let dyn_a: &dyn AnyComponent<FakeNode> = &a;
        let dyn_b: &dyn AnyComponent<FakeNode> = &b;

        assert_eq!(ComponentDiff::Equal, dyn_a.compare(dyn_b.as_any()));
    }
}
