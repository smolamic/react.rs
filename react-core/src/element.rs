use crate::{
    component::{AnyComponent, Component},
    node::Node,
};
use std::sync::Arc;

#[derive(Debug)]
pub enum Element<N>
where
    N: Node,
{
    Component(Arc<dyn AnyComponent<N>>),
    Node(Arc<N>),
    Text(Arc<String>),
    Fragment(Vec<Element<N>>),
}

impl<N> Element<N>
where
    N: Node,
{
    pub fn from_component<C>(component: C) -> Self
    where
        C: Component<N> + 'static,
    {
        Self::Component(Arc::new(component))
    }
}

impl<N> PartialEq for Element<N>
where
    N: Node,
{
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Component(this), Self::Component(other)) => this.as_ref() == other.as_ref(),
            (Self::Node(this), Self::Node(other)) => this == other,
            (Self::Text(this), Self::Text(other)) => this == other,
            (Self::Fragment(children), Self::Fragment(other_children)) => {
                children == other_children
            }
            _ => false,
        }
    }
}

impl<N> From<Vec<Element<N>>> for Element<N>
where
    N: Node,
{
    fn from(elements: Vec<Element<N>>) -> Self {
        Self::Fragment(elements)
    }
}

impl<N> From<&str> for Element<N>
where
    N: Node,
{
    fn from(text: &str) -> Self {
        Self::Text(Arc::new(text.to_string()))
    }
}

impl<N> From<String> for Element<N>
where
    N: Node,
{
    fn from(text: String) -> Self {
        Self::Text(Arc::new(text))
    }
}

impl<N> From<Arc<String>> for Element<N>
where
    N: Node,
{
    fn from(text: Arc<String>) -> Self {
        Self::Text(text)
    }
}

impl<N> Clone for Element<N>
where
    N: Node,
{
    fn clone(&self) -> Self {
        match self {
            Self::Component(component) => Self::Component(Arc::clone(component)),
            Self::Node(node) => Self::Node(Arc::clone(node)),
            Self::Text(text) => Self::Text(Arc::clone(text)),
            Self::Fragment(children) => Self::Fragment(children.clone()),
        }
    }
}

#[cfg(test)]
mod tests {
    //use super::*;
}
