use crate::element::Element;
use std::{fmt, sync::Arc};

pub trait Node: PartialEq + fmt::Debug {}

impl<N> From<N> for Element<N>
where
    N: Node,
{
    fn from(node: N) -> Self {
        Self::Node(Arc::new(node))
    }
}

pub mod fakes {
    use super::*;

    #[derive(PartialEq, Debug)]
    pub struct FakeNode;

    impl Node for FakeNode {}
}

#[cfg(test)]
pub(crate) mod tests {
    use super::*;
    use crate::element::Element;

    #[test]
    fn element_from_node() {
        let element: Element<fakes::FakeNode> = fakes::FakeNode.into();
        assert_eq!(element, Element::Node(Arc::new(fakes::FakeNode)));
    }
}
