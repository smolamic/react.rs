pub use async_channel::*;

#[derive(Debug)]
pub struct SendOrDropError<T>(T);

pub trait SenderExt<T> {
    fn send_or_drop(&self, msg: T) -> Result<(), SendOrDropError<T>>;
}

impl<T> SenderExt<T> for Sender<T> {
    fn send_or_drop(&self, msg: T) -> Result<(), SendOrDropError<T>> {
        match self.try_send(msg) {
            Ok(()) => Ok(()),
            Err(TrySendError::Full(_)) => Ok(()),
            Err(TrySendError::Closed(msg)) => Err(SendOrDropError(msg)),
        }
    }
}
