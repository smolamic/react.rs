use react_core::{use_state, Component, Hook};
use react_html::{tag, Callback, HtmlElement, HtmlNode};
use wasm_bindgen::prelude::*;

// When the `wee_alloc` feature is enabled, this uses `wee_alloc` as the global
// allocator.
//
// If you don't want to use `wee_alloc`, you can safely delete this.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[derive(PartialEq, Debug)]
struct Counter;

impl Component<HtmlNode> for Counter {
    fn render(&self, hook: &mut Hook) -> HtmlElement {
        let (count, setter) = use_state(hook, 0);

        tag::div()
            .children(vec![
                tag::p()
                    .children(vec![format!("Count is {}", count).into()])
                    .build()
                    .into(),
                tag::button()
                    .on(
                        "click",
                        Callback::new(move |_| setter.update(|count| count + 1)),
                    )
                    .children(vec!["Increase".into()])
                    .build()
                    .into(),
            ])
            .build()
            .into()
    }
}

impl From<Counter> for HtmlElement {
    fn from(counter: Counter) -> Self {
        HtmlElement::from_component(counter)
    }
}

// This is like the `main` function, except for JavaScript.
#[wasm_bindgen(start)]
pub fn main_js() -> Result<(), JsValue> {
    // This provides better error messages in debug mode.
    // It's disabled in release mode so it doesn't bloat up the file size.
    #[cfg(debug_assertions)]
    console_error_panic_hook::set_once();

    let target = web_sys::window()
        .unwrap()
        .document()
        .unwrap()
        .get_element_by_id("root")
        .unwrap();

    let counter: HtmlElement = Counter.into();

    react_dom::render(counter, target.into());

    Ok(())
}
