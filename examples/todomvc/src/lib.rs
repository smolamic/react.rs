mod to_do_list;

use react_core::{use_model, Component, Hook};
use react_html::{tag, Callback, HtmlElement, HtmlNode};
use to_do_list::{Msg, ToDoList};
use wasm_bindgen::prelude::*;

// When the `wee_alloc` feature is enabled, this uses `wee_alloc` as the global
// allocator.
//
// If you don't want to use `wee_alloc`, you can safely delete this.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[derive(PartialEq, Debug)]
struct MainView;

impl Component<HtmlNode> for MainView {
    fn render(&self, hook: &mut Hook) -> HtmlElement {
        let (todos, dispatcher) = use_model::<ToDoList>(hook);
        let (text, text_dispatcher) = use_model::<String>(hook);

        tag::div()
            .children(vec![
                tag::input().type_("text").value(text).build().into(),
                tag::ul()
                    .children(
                        todos
                            .get()
                            .iter()
                            .map(|item| {
                                tag::li()
                                    .children(vec![item.0.to_string().into()])
                                    .build()
                                    .into()
                            })
                            .collect(),
                    )
                    .build()
                    .into(),
            ])
            .build()
            .into()
    }
}

impl From<MainView> for HtmlElement {
    fn from(main_view: MainView) -> Self {
        HtmlElement::from_component(main_view)
    }
}

// This is like the `main` function, except for JavaScript.
#[wasm_bindgen(start)]
pub fn main_js() -> Result<(), JsValue> {
    // This provides better error messages in debug mode.
    // It's disabled in release mode so it doesn't bloat up the file size.
    #[cfg(debug_assertions)]
    console_error_panic_hook::set_once();

    let target = web_sys::window()
        .unwrap()
        .document()
        .unwrap()
        .get_element_by_id("root")
        .unwrap();

    let button: HtmlElement = MainView.into();

    react_dom::render(button, target.into());

    Ok(())
}
