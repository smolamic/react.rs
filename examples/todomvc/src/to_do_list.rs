use react_core::Model;
use std::sync::Arc;

type ToDo = (String, bool);

#[derive(Clone, PartialEq, Default)]
pub(crate) struct ToDoList(Vec<ToDo>);

#[derive(Clone)]
pub(crate) enum Msg {
    Add(String),
    Toggle(String),
}

impl Model for ToDoList {
    type Msg = Msg;

    fn update(mut self: Arc<Self>, msg: Self::Msg) -> Arc<Self> {
        match msg {
            Msg::Add(text) => {
                Arc::make_mut(&mut self).0.push((text, false));
            }

            Msg::Toggle(text) => {
                let todos: &mut Vec<ToDo> = &mut Arc::make_mut(&mut self).0;

                if let Some(todo) = todos.iter_mut().find(|(cur, _)| cur == &text) {
                    todo.1 = !todo.1;
                }
            }
        }
        self
    }
}

impl ToDoList {
    pub fn get(&self) -> &Vec<ToDo> {
        &self.0
    }
}
